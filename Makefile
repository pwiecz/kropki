CXXFLAGS=-march=native -fno-rtti -fno-exceptions -O3 -DNDEBUG -Wall -Wextra -pedantic -Wno-unused-variable -Wno-unused-but-set-variable -Wno-unused-parameter -Werror=switch -std=c++17
CXXFLAGS_DBG=-O0 -g -Wall -Wextra -pedantic -Wno-unused-variable -Wno-unused-but-set-variable -Wno-unused-parameter	 -std=c++17
FLTK_CXXFLAGS=$(CXXFLAGS) `fltk-config --cxxflags`
FLTK_LDFLAGS=`fltk-config --ldflags`

BINARIES=kropki_random_test kropki_test ui/random_test ui/play ui/self_play ui/kropki mcts_benchmark mcts_uct0_benchmark mcts_uct0_random_test mcts_uct1_benchmark compare_engines

all: $(BINARIES)

clean:
	rm -f *.o $(BINARIES)

kropki_random_test.o: kropki_random_test.cc kropki.h kropki_parent.h static_vector.h pieces.h
	g++ $(CXXFLAGS) -c -o $@ $<
kropki_random_test: kropki_random_test.o
	g++ $(CXXFLAGS) -o $@ $^

kropki_random_test_dbg.o: kropki_random_test.cc kropki.h kropki_parent.h
	g++ $(CXXFLAGS_DBG) -c -o $@ $<
kropki_random_test_dbg: kropki_random_test_dbg.o
	g++ $(CXXFLAGS_DBG) -o $@ $^

catch_config_runner.o: catch_config_runner.cc
	g++ $(CXXFLAGS) -c -o $@ $<

kropki_test.o: kropki_test.cc kropki.h
	g++ $(CXXFLAGS) -c -o $@ $<

kropki_test: catch_config_runner.o kropki_test.o
	g++ $(CXXFLAGS) -o $@ $^

ui/kropki_window.cxx: ui/kropki_window.fl
	fluid -c -o ui/kropki_window.cxx -h ui/kropki_window.h ui/kropki_window.fl

ui/random_test: ui/random_test.cc kropki.h
	g++ $(FLTK_CXXFLAGS) -o $@ $< $(FLTK_LDFLAGS)

ui/play: ui/play.cc ui/board.h mcts_engines.h mcts_uct0.h mcts_uct1.h kropki.h
	g++ $(FLTK_CXXFLAGS) -o $@ $< $(FLTK_LDFLAGS) -lpthread

ui/self_play: ui/self_play.cc ui/board.h mcts_engines.h mcts_uct0.h mcts_uct1.h kropki.h
	g++ $(FLTK_CXXFLAGS) -o $@ $< $(FLTK_LDFLAGS) -lpthread

ui/kropki_window.o: ui/kropki_window.cxx ui/kropki_window.h ui/board.h kropki_parent.h
	g++ $(FLTK_CXXFLAGS) -I. -c -o $@ $<

ui/kropki: ui/kropki.cxx ui/kropki.h ui/kropki_window.o ui/board.h
	g++ $(FLTK_CXXFLAGS) -I. -o $@ $< ui/kropki_window.o $(FLTK_LDFLAGS)

mcts_benchmark: mcts_benchmark.cc kropki.h mcts.h
	g++ $(CXXFLAGS) -o $@ $<

mcts_uct0_benchmark: mcts_uct0_benchmark.cc mcts_uct0.h kropki.h kropki_parent.h
	g++ $(CXXFLAGS) -o $@ $<

mcts_uct0_random_test: mcts_uct0_benchmark.cc mcts_uct0.h kropki.h kropki_parent.h
	g++ $(CXXFLAGS_DBG) -o $@ $<

mcts_uct1_benchmark: mcts_uct1_benchmark.cc mcts_uct1.h kropki.h
	g++ $(CXXFLAGS) -o $@ $<

compare_engines: compare_engines.cc mcts_engines.h mcts_uct0.h mcts_uct1.h kropki.h
	g++ $(CXXFLAGS) -o $@ $<
