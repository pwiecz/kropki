#include <type_traits>

#define CATCH_CONFIG_RUNNER
#define CATCH_CONFIG_DISABLE_EXCEPTIONS
#include "catch.hpp"
#include "pcg_random.hpp"

//#include "kropki.h"


//using kropki::Board;

int main(int argc, char* argv[]) {
  std::cerr << "Initializing hashes\n";
  constexpr uint16_t W = 32, H = 16;
  pcg64 rand;
  //  Board<W, H>::InitializeHashes(rand);
  //static_assert(std::is_trivially_copyable<Board<W, H>>::value, "Board no trivially copyable");

  return Catch::Session().run(argc, argv);
}
