#include "mcts_engines.h"

#include <cstdio>
#include <cstdlib>
#include <stdexcept>
#include <execinfo.h>


using kropki::Board;

void handler() {
  void *trace_elems[20];
    int trace_elem_count(backtrace( trace_elems, 20 ));
    char **stack_syms(backtrace_symbols( trace_elems, trace_elem_count ));
    for ( int i = 0 ; i < trace_elem_count ; ++i )
    {
      printf("%s\n", stack_syms[i]);
    }
    free( stack_syms );

    exit(1);
}   

int main() {
  std::set_terminate(handler);

  constexpr uint16_t W = 10, H = 10;
  pcg64 rand;
  Board<W, H>::InitializeHashes(rand);
  
    
  uint16_t uct0Score = 0;
  uint16_t uct1Score = 0;
  for (int i = 0; i < 100; ++i) {
    fprintf(stderr, "Match %d\n", i+1);
    Board<W, H> board;
    MCTS_UCT0_Engine<Board<W, H>> eng0;
    std::string eng0Name = "MCTS_UCT0";
    //    RandomEngine<W, H> uct0;
    MCTS_UCT1_Engine<Board<W, H>> eng1;
    std::string eng1Name = "MCTS_UCT1";

    uint16_t moveNo = i % 2;
    while (true) {
      if (moveNo % 2 == 0) {
	uint32_t iters = eng0.ThinkForTime(std::chrono::milliseconds(1000));
	//	fprintf(stderr, "%s performed %d iterations\n", eng0Name.c_str(), (int)iters);
	const uint16_t whiteMove = eng0.BestMove();
	if (whiteMove > 0) {
	  eng0.Place(whiteMove);
	  eng1.Place(whiteMove);
	  board.Place(whiteMove);
	} else {
	  fprintf(stderr, "%s resigns\n", eng0Name.c_str());
	  ++uct1Score;
	  break;
	}
	if (board.NumEmptyFields == 0) {
	  int8_t res = -board.Result();
	  if (res > 0) {
	    fprintf(stderr, "%s wins\n", eng0Name.c_str());
	    ++uct0Score;
	  } else if (res < 0) {
	    fprintf(stderr, "%s wins\n", eng1Name.c_str());
	    ++uct1Score;
	  } else {
	    fprintf(stderr, "Draw\n");
	  }
	  break;
	}
      } else {
	uint32_t iters = eng1.ThinkForTime(std::chrono::milliseconds(1000));
	//	std::cerr << eng1Name << " performed " << iters << " iterations\n";
	const uint16_t blackMove = eng1.BestMove();
	if (blackMove > 0) {
	  eng0.Place(blackMove);
	  eng1.Place(blackMove);
	  board.Place(blackMove);
	} else {
	  fprintf(stderr, "%s resigns\n", eng1Name.c_str());
	  ++uct1Score;
	  break;
	}
	if (board.NumEmptyFields == 0) {
	  int8_t res = -board.Result();
	  if (res > 0) {
	    fprintf(stderr, "%s wins\n", eng1Name.c_str());
	    ++uct1Score;
	  } else if (res < 0) {
	    fprintf(stderr, "%s wins\n", eng0Name.c_str());
	    ++uct0Score;
	  } else {
	    fprintf(stderr, "Draw\n");
	  }
	  break;
	}
      }
      ++moveNo;
    }
    fprintf(stderr, "Score: %s %d vs %d %s\n", eng0Name.c_str(), (int)uct0Score, (int)uct1Score, eng1Name.c_str());
  }
}
