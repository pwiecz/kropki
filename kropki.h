#pragma once

#include <array>
#include <cstdint>
#include <vector>

#include <string>

#include "pcg_random.hpp"

#include "static_vector.h"
#include "assert.h"

#include "pieces.h"


namespace kropki {

enum class FillMode {
  SURROUND, CAPTURE, EMPTY,
};

template<uint16_t W, uint16_t H>
class Board {
  static_assert(W < 128 && H < 128, "Board dimensions are too large");
  static_assert(W > 0 && H > 0, "Board size must be greater than zero");
public:
  static constexpr uint16_t Width = W;
  static constexpr uint16_t Height = H;

  Board();
  Board(const Board<W, H>& board) = default;

  template<class Rand>
  static void InitializeHashes(Rand& rand);

  void Place(uint16_t x, uint16_t y, Side side);
  void Place(uint16_t x, uint16_t y);
  void Place(uint16_t index);

  void MakeCanonical();

  int8_t Result() const;

  FieldState PieceAt(uint16_t x, uint16_t y) const;

  std::vector<uint16_t> GetEmptyFields() const;
  void GetEmptyFields(std::vector<uint16_t>* emptyFields) const;
  uint16_t GetNthEmptyField(uint16_t n) const;

  bool CanBePlayed(uint8_t x, uint8_t y) const;

  static uint16_t CoordsToIndex(uint16_t x, uint16_t y);
  static std::string IndexToString(uint16_t index);
  static void IndexToCoords(uint16_t index, uint16_t&x, uint16_t& y);
  static uint16_t IndexToX(uint16_t index);
  static uint16_t IndexToY(uint16_t index);
  static bool IndexOnBoard(uint16_t index);


  // Types on pieces on board.
  std::array<FieldState, (W+2)*(H+2)> Pieces = std::array<FieldState, (W+2)*(H+2)>{};
  // Non-empty fields contain index of the root of the chain it belong to.
  // Empty fields contain index of the next empty field.
  std::array<uint16_t, (W+2)*(H+2)> Chains = std::array<uint16_t, (W+2)*(H+2)>{};
  // Non-empty, non-root fields contain winding number of the path to the root.
  // (roots of chains contain size of the chain).
  // Empty fields contain index of the previous empty field.
  std::array<int16_t, (W+2)*(H+2)> Winding = std::array<int16_t, (W+2)*(H+2)>{};

  Side SideToMove = Side::WHITE;

  uint16_t NumEmptyFields = W * H;
  uint16_t FirstEmptyField;

  int16_t WhiteCaptured = 0;
  int16_t BlackCaptured = 0;
  
  //  uint64_t ZobristHash = 0;

  static std::vector<std::vector<uint64_t>> Hashes;
private:
  void Place(uint16_t index, Side side);

  // Returns true iff we make a closed loop.
  template<Side>
  bool MarkSurroundedAreas(uint16_t index, bool capture, static_vector<uint8_t, 8>& sameColorNeighbours);
  template<Side, FillMode>
  void FloodFillFrom(uint16_t index);
  template<FillMode>
  void FloodFillFromWhite(uint16_t index);
  // Returns true iff we surround an enemy (black) piece.
  bool SurroundByWhitePieces(uint16_t index);
  void CaptureByWhitePieces(uint16_t index);
  void ClearBlackSurrounding(uint16_t index);
  template<FillMode>
  void FloodFillFromBlack(uint16_t index);
  // Returns true iff we surround an enemy (black) piece.
  bool SurroundByBlackPieces(uint16_t index);
  void CaptureByBlackPieces(uint16_t index);
  void ClearWhiteSurrounding(uint16_t index);

  template<Side>
  void MergeAdjacentChains(uint16_t index, const static_vector<uint8_t, 8>& sameColorNeighbours);
  template<Side>
  void MakeRoot(uint16_t index, uint16_t currentChain);
  template<Side>
  void ReRootPiecesTo(uint16_t sourceChain, uint16_t targetChain, uint16_t currentIndex, int16_t currentWinding, uint8_t chainDirection);

  void RemoveEmptyField(uint16_t index);

  static constexpr std::array<int16_t, 8> indicesAround{-(W+2)-1, -(W+2), -(W+2)+1, 1, (W+2)+1, W+2, (W+2)-1, -1};
  static constexpr std::array<int16_t, 8> dxs{-1, 0, 1, 1, 1, 0, -1, -1};
  static constexpr std::array<int16_t, 8> dys{-1, -1, -1, 0, 1, 1, 1, 0};
};

template<uint16_t W, uint16_t H>
Board<W, H>::Board() 
  : FirstEmptyField(CoordsToIndex(0, 0)) {
  for (uint16_t x = 0; x < W; ++x) {
    const uint16_t nextX = (x < W - 1) ? x + 1 : 0;
    const uint16_t prevX = (x > 0) ? x - 1 : W - 1;
    for (uint16_t y = 0; y < H; ++y) {
      const uint16_t nextY = (x < W - 1) ? y : ((y < H - 1) ? y + 1 : 0);
      const uint16_t prevY = (x > 0) ? y : (y > 0 ? y - 1 : (H - 1));
      Chains[CoordsToIndex(x, y)] = CoordsToIndex(nextX, nextY);
      Winding[CoordsToIndex(x, y)] = (int16_t)CoordsToIndex(prevX, prevY);
    }
  }
}

template<uint16_t W, uint16_t H>
bool Board<W, H>::CanBePlayed(uint8_t x, uint8_t y) const {
  const uint16_t index = CoordsToIndex(x, y);
  if (!Board::IndexOnBoard(index)) {
    return false;
  }
  if (Pieces[index] != FieldState::EMPTY &&
      Pieces[index] != FieldState::EMPTY_SURROUNDED_BY_WHITE && 
      Pieces[index] != FieldState::EMPTY_SURROUNDED_BY_BLACK) {
    return false;
  }
  return true;
}

template<uint16_t W, uint16_t H>
void Board<W, H>::Place(uint16_t index, Side side) {
  X_ASSERT(IndexOnBoard(index));
  //  if (Pieces[index] != FieldState::EMPTY && Pieces[index] != FieldState::EMPTY_SURROUNDED_BY_WHITE && Pieces[index] != FieldState::EMPTY_SURROUNDED_BY_BLACK) {
  //    return false;
  //  }
  X_ASSERT(Pieces[index] == FieldState::EMPTY || Pieces[index] == FieldState::EMPTY_SURROUNDED_BY_WHITE || Pieces[index] == FieldState::EMPTY_SURROUNDED_BY_BLACK);
  RemoveEmptyField(index);
  bool isPlayOnSurrounded;
  if (side == Side::BLACK) {
    isPlayOnSurrounded = Pieces[index] == FieldState::EMPTY_SURROUNDED_BY_WHITE;
  } else {
    isPlayOnSurrounded = Pieces[index] == FieldState::EMPTY_SURROUNDED_BY_BLACK;
  }
  Pieces[index] = SideToFieldState(side);
  Chains[index] = index;
  Winding[index] = 1;
  X_ASSERT(!Hashes.empty());
  //  ZobristHash ^= Hashes[index][(size_t)Pieces[index]];
  static_vector<uint8_t, 8> sameColorNeighbours;
  if (side == Side::BLACK) {
    const bool closedChain = MarkSurroundedAreas<Side::BLACK>(index, isPlayOnSurrounded, sameColorNeighbours);
    if (isPlayOnSurrounded && !closedChain) {
      FloodFillFromWhite<FillMode::CAPTURE>(index);
    } else {
      MergeAdjacentChains<Side::BLACK>(index, sameColorNeighbours);
    }
  } else {
    const bool closedChain = MarkSurroundedAreas<Side::WHITE>(index, isPlayOnSurrounded, sameColorNeighbours);
    if (isPlayOnSurrounded && !closedChain) {
      FloodFillFromBlack<FillMode::CAPTURE>(index);
    } else {
      MergeAdjacentChains<Side::WHITE>(index, sameColorNeighbours);
    }
  }
}

template<uint16_t W, uint16_t H>
void Board<W, H>::Place(uint16_t index) {
  Place(index, SideToMove);
  SideToMove = OppositeSide(SideToMove);
}

template<uint16_t W, uint16_t H>
void Board<W, H>::Place(uint16_t x, uint16_t y) {
  Place(CoordsToIndex(x, y));
}

template<uint16_t W, uint16_t H>
void Board<W, H>::Place(uint16_t x, uint16_t y, Side side) {
  Place(CoordsToIndex(x, y), side);
}

template<uint16_t W, uint16_t H>
void Board<W, H>::MakeCanonical() {
  if (SideToMove == Side::WHITE) {
    return;
  }
  for (uint16_t index = 0; index < Pieces.size(); ++index) {
    const FieldState state = Pieces[index];
    const FieldState oppositeState = OppositeState(state);
    Pieces[index] = oppositeState;
    //    ZobristHash ^= Hashes[index][(size_t)state] & Hashes[index][(size_t)oppositeState];
  }
  std::swap(WhiteCaptured, BlackCaptured);
  SideToMove = Side::WHITE;
}

template<uint16_t W, uint16_t H>
std::vector<uint16_t> Board<W, H>::GetEmptyFields() const {
  std::vector<uint16_t> emptyFields(NumEmptyFields);
  if (NumEmptyFields > 0) {
    size_t curI = 0;
    uint16_t cur = FirstEmptyField;
    do {
      X_ASSERT(curI < NumEmptyFields);
      emptyFields[curI] = cur;
      cur = Chains[cur];
      ++curI;
    } while (cur != FirstEmptyField);
    X_ASSERT(curI == NumEmptyFields);
  }
  X_ASSERT(NumEmptyFields == emptyFields.size());
  return emptyFields;
}

template<uint16_t W, uint16_t H>
void Board<W, H>::GetEmptyFields(std::vector<uint16_t>* emptyFields) const {
  X_ASSERT(emptyFields->size() == NumEmptyFields);
  if (NumEmptyFields > 0) {
    size_t curI = 0;
    uint16_t cur = FirstEmptyField;
    do {
      X_ASSERT(curI < NumEmptyFields);
      (*emptyFields)[curI] = cur;
      cur = Chains[cur];
      ++curI;
    } while (cur != FirstEmptyField);
    X_ASSERT(curI == NumEmptyFields);
  }
}

template<uint16_t W, uint16_t H>
uint16_t Board<W, H>::GetNthEmptyField(uint16_t n) const {
  X_ASSERT(NumEmptyFields > 0);
  uint16_t result = FirstEmptyField;
  while (n > 0) {
    result = Chains[result];
    --n;
  }
  return result;
}

template<uint16_t W, uint16_t H>
void Board<W, H>::RemoveEmptyField(uint16_t index) {
  X_ASSERT(NumEmptyFields > 0);
  --NumEmptyFields;
  const uint16_t nextEmptyField = Chains[index];
  const uint16_t prevEmptyField = (uint16_t)Winding[index];
  X_ASSERT(NumEmptyFields == 0 || FieldState::EMPTY == Pieces[nextEmptyField] || FieldState::EMPTY_SURROUNDED_BY_BLACK == Pieces[nextEmptyField] || FieldState::EMPTY_SURROUNDED_BY_WHITE == Pieces[nextEmptyField]);
  X_ASSERT(NumEmptyFields == 0 || FieldState::EMPTY == Pieces[prevEmptyField] || FieldState::EMPTY_SURROUNDED_BY_BLACK == Pieces[prevEmptyField] || FieldState::EMPTY_SURROUNDED_BY_WHITE == Pieces[prevEmptyField]);
  Winding[nextEmptyField] = (int16_t)prevEmptyField;
  Chains[prevEmptyField] = nextEmptyField;
  if (index == FirstEmptyField) {
    FirstEmptyField = nextEmptyField;
  }
}

template<uint16_t W, uint16_t H>
template<Side side>
bool Board<W, H>::MarkSurroundedAreas(uint16_t index, bool isPlayOnSurrounded, static_vector<uint8_t, 8>& sameColorNeighbours) {
  X_ASSERT(IndexOnBoard(index));
  static_vector<uint8_t, 4> separatedGroupBegins;
  static_vector<uint8_t, 4> separatedGroupEnds;
  int8_t requiredSeparation = 0;
  for (uint8_t i = 0; i < 8; ++i) {
    const uint16_t neighbourIndex = index + indicesAround[i];
    const FieldState neighbourPiece = Pieces[neighbourIndex];
    if (!PieceIsOfSide<side>(neighbourPiece)) {
      --requiredSeparation;
      continue;
    }
    sameColorNeighbours.push_back(i);
    if (requiredSeparation > 0) {
      separatedGroupEnds.back() = i;
      requiredSeparation = 1 + (i%2);
      continue;
    }
    separatedGroupBegins.push_back(i);
    separatedGroupEnds.push_back(i);
    requiredSeparation = 1 + (i%2);
  }
  if (separatedGroupBegins.size() <= 1) {
    return false;
  }
  
  if (separatedGroupEnds.back() == 7 && separatedGroupBegins.front() <= 1) {
    separatedGroupBegins.front() = separatedGroupBegins.back();
    separatedGroupBegins.pop_back();
    separatedGroupEnds.pop_back();
  }
  if (separatedGroupBegins.size() <= 1) {
    return false;
  }

  bool closedChain = false;
  const int16_t y = (int16_t)IndexToY(index);
  for (uint8_t i = 0; i < separatedGroupBegins.size(); ++i) {
    const uint8_t si = separatedGroupBegins[i];
    const uint16_t iIndex = index + indicesAround[si];
    const uint16_t iChain = Chains[iIndex];
    const int16_t iWinding = ((iIndex == iChain) ? 0 : Winding[iIndex]) + dxs[si] * (y + y + dys[si]);
    for (uint8_t j = i+1; j < separatedGroupBegins.size(); ++j) {
      const uint8_t sj = separatedGroupBegins[j];
      const uint16_t jIndex = index + indicesAround[sj];
      const uint16_t jChain = Chains[jIndex];
      if (iChain != jChain) {
	continue;
      }
      const int16_t jWinding = ((jIndex == jChain) ? 0 : Winding[jIndex]) + dxs[sj] * (y + y + dys[sj]);
      X_ASSERT(iWinding != jWinding);
      if (jWinding - iWinding > 0) {
	for (uint8_t k = (separatedGroupEnds[i]+1)%8; k != separatedGroupBegins[j]; k=(k+1)%8) {
	  closedChain = true;
	  if (isPlayOnSurrounded) {
	    FloodFillFrom<side, FillMode::CAPTURE>(index + indicesAround[k]);
	  } else {
	    FloodFillFrom<side, FillMode::SURROUND>(index + indicesAround[k]);
	  }
	}
	i = j;
      } else {
	for (uint8_t k = (separatedGroupEnds[j]+1)%8; k != separatedGroupBegins[i]; k=(k+1)%8) {
	  closedChain = true;
	  if (isPlayOnSurrounded) {
	    FloodFillFrom<side, FillMode::CAPTURE>(index + indicesAround[k]);
	  } else {
	    FloodFillFrom<side, FillMode::SURROUND>(index + indicesAround[k]);
	  }
	}
	goto loop_end;
      }
    }
  }
 loop_end:
  if (isPlayOnSurrounded && closedChain) {
    for (uint8_t i : {1, 3, 5, 7}) {
      FloodFillFrom<side, FillMode::EMPTY>(index + indicesAround[i]);
    }
  }
  return closedChain;
}

template<uint16_t W, uint16_t H>
template<Side side>
void Board<W, H>::MergeAdjacentChains(uint16_t index, const static_vector<uint8_t, 8>& sameColorNeighbours) {
  X_ASSERT(IndexOnBoard(index));
  uint16_t largestNeighbourChain = 0;
  uint16_t largestNeighbourChainI = 0;
  uint16_t largestNeighbourChainSize = 0;
  for (uint8_t i : sameColorNeighbours) {
    const uint16_t neighbourIndex = index + indicesAround[i];
    const FieldState neighbourPiece = Pieces[neighbourIndex];
    if (PieceIsOfSide<side>(neighbourPiece)) {
      const uint16_t neighbourChain = Chains[neighbourIndex];
      const uint16_t neighbourChainSize = Winding[neighbourChain];
      // Prioritize neighbours in cardinal directions to minimize risk of 
      // crossing streams.
      if (neighbourChainSize > largestNeighbourChainSize || 
	  (neighbourChainSize == largestNeighbourChainSize && largestNeighbourChainI % 2 == 0 && i % 2 == 1)) {
						   
	largestNeighbourChain = neighbourChain;
	largestNeighbourChainI = i;
	largestNeighbourChainSize = neighbourChainSize;
      }
    }
  }
  if (largestNeighbourChainSize == 0) {
    return;
  }
  const uint16_t largestNeighbourChainIndex = index + indicesAround[largestNeighbourChainI];
  X_ASSERT(Winding[Chains[index]] > 0);
  //  --Winding[Chains[index]];
  Chains[index] = largestNeighbourChain;
  ++Winding[largestNeighbourChain];
  const int16_t dx = dxs[largestNeighbourChainI], dy = dys[largestNeighbourChainI];
  const uint16_t y = IndexToY(index);
  const int16_t winding = ((largestNeighbourChainIndex == largestNeighbourChain) ? 0 : Winding[largestNeighbourChainIndex]) + dx * (y + y + dy);
  Winding[index] = winding;

  for (uint8_t neighbourI : sameColorNeighbours) {
    const uint16_t neighbourIndex = index + indicesAround[neighbourI];
    const uint16_t neighbourChain = Chains[neighbourIndex];
    if (neighbourChain == largestNeighbourChain) {
      continue;
    }
    ReRootPiecesTo<side>(neighbourChain, largestNeighbourChain, neighbourIndex, winding, neighbourI);
  }
}

template<uint16_t W, uint16_t H>
template<Side side, FillMode mode>
void Board<W, H>::FloodFillFrom(uint16_t index) {
  if constexpr(side == Side::WHITE) {
    FloodFillFromWhite<mode>(index);
  } else {
    FloodFillFromBlack<mode>(index);
  }
}

template<uint16_t W, uint16_t H>
template<FillMode mode>
void Board<W, H>::FloodFillFromWhite(uint16_t index) {
  if constexpr(mode == FillMode::CAPTURE) {
    CaptureByWhitePieces(index);
  } else if constexpr(mode == FillMode::EMPTY) {
    ClearBlackSurrounding(index);
  } else {
    if (SurroundByWhitePieces(index)) {
      CaptureByWhitePieces(index);
    }
  }
}

template<uint16_t W, uint16_t H>
bool Board<W, H>::SurroundByWhitePieces(uint16_t index) {
 bool capture = false;
  X_ASSERT(IndexOnBoard(index));
  //  size_t xorHash = Hashes[index][(size_t)Pieces[index]];
  switch(Pieces[index]) {
  case FieldState::EMPTY:
    Pieces[index] = FieldState::EMPTY_SURROUNDED_BY_WHITE;
    break;
  case FieldState::BLACK:
    return true;
  case FieldState::EMPTY_SURROUNDED_BY_WHITE: [[fallthrough]];
  case FieldState::WHITE: [[fallthrough]];
  case FieldState::CAPTURED_BLACK: [[fallthrough]];
  case FieldState::EMPTY_CAPTURED_BY_WHITE:
    return false;
  case FieldState::CAPTURED_WHITE:
    X_ASSERT(false);
    return false;
  case FieldState::EMPTY_SURROUNDED_BY_BLACK:
    X_ASSERT(false);
    return false;
  case FieldState::EMPTY_CAPTURED_BY_BLACK:
    X_ASSERT(false);
    return false;
  default:
    X_ASSERT(false);
    return false;
  }
  //  ZobristHash ^= xorHash ^ Hashes[index][(size_t)Pieces[index]];
  X_ASSERT(IndexToX(index) != 0);
  X_ASSERT(IndexToX(index) != (W + 1));
  X_ASSERT(IndexToY(index) != 0);
  X_ASSERT(IndexToY(index) != (H + 1));
  return SurroundByWhitePieces(index - 1) ||
    SurroundByWhitePieces(index + 1) ||
    SurroundByWhitePieces(index - (W + 2)) || 
    SurroundByWhitePieces(index + (W + 2));
}

template<uint16_t W, uint16_t H>
void Board<W, H>::CaptureByWhitePieces(uint16_t index) {
  bool capture = false;
  X_ASSERT(IndexOnBoard(index));
  //  size_t xorHash = Hashes[index][(size_t)Pieces[index]];
  switch(Pieces[index]) {
  case FieldState::EMPTY: [[fallthrough]];
  case FieldState::EMPTY_SURROUNDED_BY_WHITE: [[fallthrough]];
  case FieldState::EMPTY_SURROUNDED_BY_BLACK:
    Pieces[index] = FieldState::EMPTY_CAPTURED_BY_WHITE;
    RemoveEmptyField(index);
    break;
  case FieldState::EMPTY_CAPTURED_BY_BLACK:
    Pieces[index] = FieldState::EMPTY_CAPTURED_BY_WHITE;
    break;
  case FieldState::BLACK:
    Pieces[index] = FieldState::CAPTURED_BLACK;
    ++BlackCaptured;
    capture = true;
    break;
  case FieldState::CAPTURED_WHITE:
    Pieces[index] = FieldState::WHITE;
    X_ASSERT(WhiteCaptured > 0);
    --WhiteCaptured;
    break;
  case FieldState::WHITE: [[fallthrough]];
  case FieldState::CAPTURED_BLACK: [[fallthrough]];
  case FieldState::EMPTY_CAPTURED_BY_WHITE:
    return;
  default:
    X_ASSERT(false);
  }
  //  ZobristHash ^= xorHash ^ Hashes[index][(size_t)Pieces[index]];
  X_ASSERT(IndexToX(index) != 0);
  X_ASSERT(IndexToX(index) != (W + 1));
  X_ASSERT(IndexToY(index) != 0);
  X_ASSERT(IndexToY(index) != (H + 1));
  CaptureByWhitePieces(index - 1);
  CaptureByWhitePieces(index + 1);
  CaptureByWhitePieces(index - (W + 2));
  CaptureByWhitePieces(index + (W + 2));
  if (capture) {
    X_ASSERT(Pieces[index] == FieldState::CAPTURED_BLACK);
    const uint16_t chain = Chains[index];
    // Check neighbours in cardinal direction to see if they break the opposite chain.
    for (uint8_t i = 1; i < 8; i += 2) {
      const uint16_t neighbourIndex = index + indicesAround[i];
      const FieldState neighbourState = Pieces[neighbourIndex];
      const uint16_t nextNeighbourIndex = index + indicesAround[(i+1)%8];
      const FieldState nextNeighbourState = Pieces[nextNeighbourIndex];
      const uint16_t nextNeighbourChain = Chains[nextNeighbourIndex];
      const uint16_t nextNextNeighbourIndex = index + indicesAround[(i+2)%8];
      const FieldState nextNextNeighbourState = Pieces[nextNextNeighbourIndex];
      if (neighbourState == FieldState::WHITE &&
	  nextNeighbourState == FieldState::BLACK && nextNextNeighbourState == FieldState::WHITE &&
	  chain == nextNeighbourChain) {
	if (nextNeighbourChain != nextNeighbourIndex) {
	  MakeRoot<Side::BLACK>(nextNeighbourIndex, nextNeighbourChain);
	}
      }
    }
    // This piece is or soon will be cut off from the chain root so don't touch its counter.
    Chains[index] = index;
    Winding[index] = 1;
  }
}

template<uint16_t W, uint16_t H>
void Board<W, H>::ClearBlackSurrounding(uint16_t index) {
  X_ASSERT(IndexOnBoard(index));
  //  size_t xorHash = Hashes[index][(size_t)Pieces[index]];
  switch(Pieces[index]) {
  case FieldState::EMPTY_SURROUNDED_BY_BLACK:
    Pieces[index] = FieldState::EMPTY;
    break;
  case FieldState::EMPTY: [[fallthrough]];
  case FieldState::WHITE: [[fallthrough]];
  case FieldState::BLACK: [[fallthrough]];
  case FieldState::CAPTURED_BLACK: [[fallthrough]];
  case FieldState::EMPTY_CAPTURED_BY_WHITE:
    return;
  case FieldState::CAPTURED_WHITE:
    X_ASSERT(false);
    return;
  case FieldState::EMPTY_SURROUNDED_BY_WHITE:
    X_ASSERT(false);
    return;
  case FieldState::EMPTY_CAPTURED_BY_BLACK:
    X_ASSERT(false);
    return;
  default:
    X_ASSERT(false);
    return;
  }
  //  ZobristHash ^= xorHash ^ Hashes[index][(size_t)Pieces[index]];
  X_ASSERT(IndexToX(index) != 0);
  X_ASSERT(IndexToX(index) != (W + 1));
  X_ASSERT(IndexToY(index) != 0);
  X_ASSERT(IndexToY(index) != (H + 1));
  ClearBlackSurrounding(index - 1);
  ClearBlackSurrounding(index + 1);
  ClearBlackSurrounding(index - (W + 2));
  ClearBlackSurrounding(index + (W + 2));
}

template<uint16_t W, uint16_t H>
template<FillMode mode>
void Board<W, H>::FloodFillFromBlack(uint16_t index) {
  if constexpr(mode == FillMode::CAPTURE) {
    CaptureByBlackPieces(index);
  } else if constexpr(mode == FillMode::EMPTY) {
    ClearWhiteSurrounding(index);  
  } else if constexpr(mode == FillMode::SURROUND) {
    if (SurroundByBlackPieces(index)) {
      CaptureByBlackPieces(index);
    }
  }
}

template<uint16_t W, uint16_t H>
bool Board<W, H>::SurroundByBlackPieces(uint16_t index) {
  //  size_t xorHash = Hashes[index][(size_t)Pieces[index]];
  switch(Pieces[index]) {
  case FieldState::EMPTY:
    Pieces[index] = FieldState::EMPTY_SURROUNDED_BY_BLACK;
    break;
  case FieldState::WHITE:
    return true;
  case FieldState::BLACK: [[fallthrough]];
  case FieldState::CAPTURED_WHITE: [[fallthrough]];
  case FieldState::EMPTY_SURROUNDED_BY_BLACK: [[fallthrough]];
  case FieldState::EMPTY_CAPTURED_BY_BLACK:
    return false;
  case FieldState::CAPTURED_BLACK:
    X_ASSERT(false);
    return false;
  case FieldState::EMPTY_SURROUNDED_BY_WHITE:
    X_ASSERT(false);
    return false;
  case FieldState::EMPTY_CAPTURED_BY_WHITE:
    X_ASSERT(false);
    return false;
  default:
    X_ASSERT(false);
  }
  //  ZobristHash ^= xorHash ^ Hashes[index][(size_t)Pieces[index]];
  X_ASSERT(IndexToX(index) != 0);
  X_ASSERT(IndexToX(index) != (W + 1));
  X_ASSERT(IndexToY(index) != 0);
  X_ASSERT(IndexToY(index) != (H + 1));
  return SurroundByBlackPieces(index - 1) ||
    SurroundByBlackPieces(index + 1) ||
    SurroundByBlackPieces(index - (W + 2)) ||
    SurroundByBlackPieces(index + (W + 2));
}

template<uint16_t W, uint16_t H>
void Board<W, H>::CaptureByBlackPieces(uint16_t index) {
  bool capture = false;
  //  size_t xorHash = Hashes[index][(size_t)Pieces[index]];
  switch(Pieces[index]) {
  case FieldState::EMPTY: [[fallthrough]];
  case FieldState::EMPTY_SURROUNDED_BY_WHITE: [[fallthrough]];
  case FieldState::EMPTY_SURROUNDED_BY_BLACK:
    Pieces[index] = FieldState::EMPTY_CAPTURED_BY_BLACK;
    RemoveEmptyField(index);
    break;
  case FieldState::EMPTY_CAPTURED_BY_WHITE:
    Pieces[index] = FieldState::EMPTY_CAPTURED_BY_BLACK;
    break;
  case FieldState::WHITE:
    Pieces[index] = FieldState::CAPTURED_WHITE;
    ++WhiteCaptured;
    capture = true;
    break;
  case FieldState::CAPTURED_BLACK:
    Pieces[index] = FieldState::BLACK;
    X_ASSERT(BlackCaptured > 0);
    --BlackCaptured;
    break;
  case FieldState::BLACK: [[fallthrough]];
  case FieldState::CAPTURED_WHITE: [[fallthrough]];
  case FieldState::EMPTY_CAPTURED_BY_BLACK:
    return;
  default:
    X_ASSERT(false);
  }
  //  ZobristHash ^= xorHash ^ Hashes[index][(size_t)Pieces[index]];
  X_ASSERT(IndexToX(index) != 0);
  X_ASSERT(IndexToX(index) != (W + 1));
  X_ASSERT(IndexToY(index) != 0);
  X_ASSERT(IndexToY(index) != (H + 1));
  CaptureByBlackPieces(index - 1);
  CaptureByBlackPieces(index + 1);
  CaptureByBlackPieces(index - (W + 2));
  CaptureByBlackPieces(index + (W + 2));
  if (capture) {
    X_ASSERT(Pieces[index] == FieldState::CAPTURED_WHITE);
    const uint16_t chain = Chains[index];
    // Check neighbours in cardinal direction to see if they break the opposite chain.
    for (uint8_t i = 1; i < 8; i += 2) {
      const uint16_t neighbourIndex = index + indicesAround[i];
      const FieldState neighbourState = Pieces[neighbourIndex];
      const uint16_t nextNeighbourIndex = index + indicesAround[(i+1)%8];
      const FieldState nextNeighbourState = Pieces[nextNeighbourIndex];
      const uint16_t nextNeighbourChain = Chains[nextNeighbourIndex];
      const uint16_t nextNextNeighbourIndex = index + indicesAround[(i+2)%8];
      const FieldState nextNextNeighbourState = Pieces[nextNextNeighbourIndex];
      if (neighbourState == FieldState::BLACK &&
	  nextNeighbourState == FieldState::WHITE && nextNextNeighbourState == FieldState::BLACK &&
	  chain == nextNeighbourChain) {
	if (nextNeighbourChain != nextNeighbourIndex) {
	  MakeRoot<Side::WHITE>(nextNeighbourIndex, nextNeighbourChain);
	}
      }
    }
    // This piece is or soon will be cut off from the chain root so don't touch its counter.
    Chains[index] = index;
    Winding[index] = 1;
  }

}

template<uint16_t W, uint16_t H>
void Board<W, H>::ClearWhiteSurrounding(uint16_t index) {
  bool capture = false;
  //  size_t xorHash = Hashes[index][(size_t)Pieces[index]];
  switch(Pieces[index]) {
  case FieldState::EMPTY_SURROUNDED_BY_WHITE:
    Pieces[index] = FieldState::EMPTY;
    break;
  case FieldState::EMPTY: [[fallthrough]];
  case FieldState::WHITE: [[fallthrough]];
  case FieldState::BLACK: [[fallthrough]];
  case FieldState::CAPTURED_WHITE: [[fallthrough]];
  case FieldState::EMPTY_CAPTURED_BY_BLACK:
    return;
  case FieldState::CAPTURED_BLACK:
    X_ASSERT(false);
    return;
  case FieldState::EMPTY_SURROUNDED_BY_BLACK:
    X_ASSERT(false);
    return;
  case FieldState::EMPTY_CAPTURED_BY_WHITE:
    X_ASSERT(false);
    return;
  default:
    X_ASSERT(false);
  }
  //  ZobristHash ^= xorHash ^ Hashes[index][(size_t)Pieces[index]];
  X_ASSERT(IndexToX(index) != 0);
  X_ASSERT(IndexToX(index) != (W + 1));
  X_ASSERT(IndexToY(index) != 0);
  X_ASSERT(IndexToY(index) != (H + 1));
  ClearWhiteSurrounding(index - 1);
  ClearWhiteSurrounding(index + 1);
  ClearWhiteSurrounding(index - (W + 2));
  ClearWhiteSurrounding(index + (W + 2));
}

template<uint16_t W, uint16_t H>
template<Side side>
  void Board<W, H>::MakeRoot(uint16_t index, uint16_t currentChain) {
  Chains[index] = index;
  Winding[index] = 1;

  // Iterate over the neighbours and mark them as the target chain first 
  // to disallow deeper recursion to reroot them possibly causing loops,
  // which may make winding computations invalid.
  static_vector<uint8_t, 8> isToReRoot;
  for (uint8_t i = 0; i < 8; ++i) {
    const uint16_t neighbourIndex = index + indicesAround[i];
    if (Chains[neighbourIndex] == currentChain && PieceIsOfSide<side>(Pieces[neighbourIndex])) {
      Chains[neighbourIndex] = index;
      isToReRoot.push_back(i);
    }
  }
  for (uint8_t dir : isToReRoot) {
    const uint16_t neighbourIndex = index + indicesAround[dir];
    ReRootPiecesTo<side>(currentChain, index, neighbourIndex, 0, dir);
  }
}

template<uint16_t W, uint16_t H>
template<Side side>
void Board<W, H>::ReRootPiecesTo(uint16_t sourceChain, uint16_t targetChain, uint16_t currentIndex,
				 int16_t currentArea, uint8_t chainDirection) {
  // We're rerooting the whole chain so there's no point and it's dangerous to decrease 
  // the size of the sourceChain.
  Chains[currentIndex] = targetChain;
  if (targetChain != currentIndex) {
    const int16_t y = (int16_t)IndexToY(currentIndex);
    const int8_t dx = -dxs[chainDirection];
    const int8_t dy = -dys[chainDirection];
    currentArea += dx * (y + y + dy);
  }
  Winding[currentIndex] = currentArea;
  ++Winding[targetChain];
  
  // Iterate over the neighbours and mark them as the target chain first 
  // to disallow deeper recursion to reroot them possibly causing loops,
  // which may make area computations invalid.
  static_vector<uint8_t, 8> isToReRoot;
  for (uint8_t i = 0; i < 8; ++i) {
    const uint16_t neighbourIndex = currentIndex + indicesAround[i];
    if (Chains[neighbourIndex] == sourceChain && PieceIsOfSide<side>(Pieces[neighbourIndex])) {
      Chains[neighbourIndex] = targetChain;
      isToReRoot.push_back(i);
    }
  }
  for (uint8_t dir : isToReRoot) {
    const uint16_t neighbourIndex = currentIndex + indicesAround[dir];
    ReRootPiecesTo<side>(sourceChain, targetChain, neighbourIndex, currentArea, dir);
  }
}

template<uint16_t W, uint16_t H>
FieldState Board<W, H>::PieceAt(uint16_t x, uint16_t y) const {
  return Pieces[CoordsToIndex(x, y)];
}

template<uint16_t W, uint16_t H>
uint16_t Board<W, H>::CoordsToIndex(uint16_t x, uint16_t y) {
  X_ASSERT(x < W);
  X_ASSERT(y < H);
  return (y+1) * (W+2) + x+1;
}

template<uint16_t W, uint16_t H>
void Board<W, H>::IndexToCoords(uint16_t index, uint16_t& x, uint16_t& y) {
  x = IndexToX(index);
  y = IndexToY(index);
}

template<uint16_t W, uint16_t H>
std::string Board<W, H>::IndexToString(uint16_t index) {
  uint16_t x, y;
  IndexToCoords(index, x, y);
  return "(" + std::to_string(x) + "," + std::to_string(y) + ")";
}

template<uint16_t W, uint16_t H>
uint16_t Board<W, H>::IndexToX(uint16_t index) {
  return index % (W + 2) - 1;
}

template<uint16_t W, uint16_t H>
uint16_t Board<W, H>::IndexToY(uint16_t index) {
  return index / (W + 2) - 1;
}

template<uint16_t W, uint16_t H>
bool Board<W, H>::IndexOnBoard(uint16_t index) {
  uint16_t x, y;
  IndexToCoords(index, x, y);
  return x >= 0 && x < W && y >= 0 && y < H;
}

template<uint16_t W, uint16_t H>
constexpr std::array<int16_t, 8> Board<W, H>::indicesAround;
template<uint16_t W, uint16_t H>
constexpr std::array<int16_t, 8> Board<W, H>::dxs;
template<uint16_t W, uint16_t H>
constexpr std::array<int16_t, 8> Board<W, H>::dys;

template<uint16_t W, uint16_t H>
std::vector<std::vector<uint64_t>> Board<W, H>::Hashes;

template<uint16_t W, uint16_t H>
template<class Rand>
void Board<W, H>::InitializeHashes(Rand& rand) {
  X_ASSERT(Hashes.empty());
  Hashes.resize((W+2)*(H+2), std::vector<uint64_t>((size_t)FieldState::NUM_STATES, (uint64_t)0));
  //  std::seed_seq seq{1,2,3,4,5};  // TODO: check what seed is good
  //  std::mt19937_64 r(seq);
  //  pcg64 r;//(777777777);
  static_assert(sizeof(typename Rand::result_type) >= sizeof(uint64_t));
  for (uint16_t x = 0; x < W; ++x) {
    for (uint16_t y = 0; y < H; ++y) {
      for (uint8_t f = 0; f < (uint8_t)(FieldState::NUM_STATES); ++f) {
	X_ASSERT(CoordsToIndex(x, y) < Hashes.size());
	Hashes[CoordsToIndex(x, y)][f] = (uint64_t)rand();
      }
    }
  }
}

template<uint16_t W, uint16_t H>
int8_t Board<W, H>::Result() const {
  int8_t whiteResult = BlackCaptured == WhiteCaptured ? 0 : (BlackCaptured > WhiteCaptured ? 1 : -1);
  switch (SideToMove) {
  case Side::WHITE: 
    return whiteResult;
  case Side::BLACK:
    return -whiteResult;
  default:
    X_ASSERT(false);
    return 0;
  }
}

}
