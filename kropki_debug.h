#pragma once

#include <string>

#include "assert.h"

#include "pieces.h"

inline std::string SideToString(Side side) {
  switch(side) {
  case Side::WHITE: return "WHITE";
  case Side::BLACK: return "BLACK";
  }
  X_ASSERT(false); 
  return "";
}

inline std::string FieldStateToString(FieldState state) {
  switch (state) {
  case FieldState::EMPTY: return "EMPTY";
  case FieldState::WHITE: return "WHITE";
  case FieldState::BLACK: return "BLACK";
  case FieldState::CAPTURED_WHITE: return "CAPTURED_WHITE";
  case FieldState::CAPTURED_BLACK: return "CAPTURED_BLACK";
  case FieldState::EMPTY_SURROUNDED_BY_WHITE: return "EMPTY_SURROUNDED_BY_WHITE";
  case FieldState::EMPTY_SURROUNDED_BY_BLACK: return "EMPTY_SURROUNDED_BY_BLACK";
  case FieldState::EMPTY_CAPTURED_BY_WHITE: return "EMPTY_CAPTURED_BY_WHITE";
  case FieldState::EMPTY_CAPTURED_BY_BLACK: return "EMPTY_CAPTURED_BY_BLACK";
  case FieldState::NUM_STATES: return "<NUM_STATES>";
  }
  X_ASSERT(false);
  return "";
}

template<class Board>
std::string BoardToString(const Board& board) {
    std::string res;
    res.reserve((Board::Width+1)*(Board::Height+1));
    res.push_back(' ');
    for (uint16_t x = 0; x < Board::Width; ++x) {
      res.push_back('0' + (char)(x % 10));
    }
    res.push_back('\n');
    for (uint16_t y = 0; y < Board::Height; ++y) {
      res.push_back('0' + (char)y%10);
      for (uint16_t x = 0; x < Board::Width; ++x) {
	switch (board.PieceAt(x, y)) {
	case FieldState::EMPTY: res.push_back(' '); break;
	case FieldState::WHITE: res.push_back('o'); break;
	case FieldState::BLACK: res.push_back('x'); break;
	case FieldState::EMPTY_SURROUNDED_BY_WHITE: res.push_back('w'); break;
	case FieldState::EMPTY_SURROUNDED_BY_BLACK: res.push_back('b'); break;
	case FieldState::EMPTY_CAPTURED_BY_WHITE: res.push_back('W'); break;
	case FieldState::EMPTY_CAPTURED_BY_BLACK: res.push_back('B'); break;
	case FieldState::CAPTURED_WHITE: res.push_back('0'); break;
	case FieldState::CAPTURED_BLACK: res.push_back('#'); break;
	}
      }
      res.push_back('\n');
    }
    return res;
}

