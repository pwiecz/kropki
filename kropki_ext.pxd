# cython: language_level=3
# distutils: language = c++

from libc.stdint cimport (uint16_t,int8_t)
from libcpp cimport bool
from libcpp.vector cimport (vector)

cdef extern from "kropki_parent.h" namespace "kropki_parent":
  cdef cppclass Board_10_10:
    Board_10_10()
    void Place(uint16_t index)
    bool CanBePlayed(uint16_t x, uint16_t y)
    @staticmethod
    uint16_t CoordsToIndex(uint16_t x, uint16_t y);
    @staticmethod
    uint16_t IndexToX(uint16_t index);
    @staticmethod
    uint16_t IndexToY(uint16_t index);
    vector[uint16_t] GetEmptyFields()
    void Reset()
    uint16_t NumEmptyFields
    int8_t Result()
    vector[vector[int]] GetObservationVectors()
  
