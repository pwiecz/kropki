# cython: language_level=3
# distutils: language = c++

from kropki_ext cimport Board_10_10

# Create a Cython extension type which holds a C++ instance
# as an attribute and create a bunch of forwarding methods
# Python extension type.
cdef class PyBoard_10_10:
    cdef Board_10_10 c_board  # Hold a C++ instance which we're wrapping

    def __cinit__(self):
        self.c_board = Board_10_10()

    def can_be_played(self, x, y):
        return self.c_board.CanBePlayed(x, y)

    def coords_to_index(self, x, y):
        return Board_10_10.CoordsToIndex(x, y)

    def index_to_x(self, index):
        return Board_10_10.IndexToX(index)

    def index_to_y(self, index):
        return Board_10_10.IndexToY(index)

    def place(self, position):
        return self.c_board.Place(position)

    def reset(self):
        self.c_board.Reset()

    def empty_fields(self):
        return self.c_board.GetEmptyFields()

    def num_empty_fields(self):
        return self.c_board.NumEmptyFields

    def result(self):
        return self.c_board.Result()

    def observation_vectors(self):
        return self.c_board.GetObservationVectors()