#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#ifndef NDEBUG
// Just for testing uniqueness of some outputs.
#include <set>
#endif
#include <vector>

#include <string>

//#include "pcg_random.hpp"

#include "static_vector.h"
#include "xassert.h"

#include "pieces.h"


namespace kropki_parent {

enum class FillMode {
  SURROUND, CAPTURE, EMPTY,
};

template<uint16_t W, uint16_t H>
class Board {
  static_assert(W < 128 && H < 128, "Board dimensions are too large");
  static_assert(W > 0 && H > 0, "Board size must be greater than zero");
public:
  static constexpr uint16_t Width = W;
  static constexpr uint16_t Height = H;

  Board();
  Board(const Board<W, H>& board) = default;

  void Reset();
  //  template<class Rand>
  //  static void InitializeHashes(Rand& rand);

  void Place(uint16_t x, uint16_t y, Side side);
  void Place(uint16_t x, uint16_t y);
  void Place(uint16_t index);

  void MakeCanonical();

  int8_t Result() const;

  FieldState PieceAt(uint16_t x, uint16_t y) const;

  std::vector<uint16_t> GetEmptyFields() const;
  void GetEmptyFields(std::vector<uint16_t>* emptyFields) const;
  uint16_t GetNthEmptyField(uint16_t n) const;
  std::vector<std::vector<int>> GetObservationVectors() const;

  bool CanBePlayed(uint16_t index) const;
  bool CanBePlayed(uint16_t x, uint16_t y) const;

  static constexpr uint16_t CoordsToIndex(uint16_t x, uint16_t y);
  static std::string IndexToString(uint16_t index);
  static constexpr void IndexToCoords(uint16_t index, uint16_t&x, uint16_t& y);
  static constexpr uint16_t IndexToX(uint16_t index);
  static constexpr uint16_t IndexToY(uint16_t index);
  static constexpr bool IndexOnBoard(uint16_t index);
  static constexpr std::array<uint16_t, W*H> GenerateAllBoardIndices();
  //  static int16_t AreaUnder(uint16_t from, uint16_t to);

  // Types of pieces on board.
  std::array<FieldState, (W+2)*(H+2)> Pieces = std::array<FieldState, (W+2)*(H+2)>{};
  // Non-empty fields contain index of the parent in the chain it belong to.
  // Empty fields contain index of the next empty field.
  std::array<uint16_t, (W+2)*(H+2)> Chains = std::array<uint16_t, (W+2)*(H+2)>{};
  // Non-empty, non-root fields contain signed area under the path to the parent.
  // Empty fields contain index of the previous empty field.
  std::array<int16_t, (W+2)*(H+2)> Area = std::array<int16_t, (W+2)*(H+2)>{};
  static constexpr std::array<uint16_t, W*H> AllBoardIndices = GenerateAllBoardIndices();

  Side SideToMove = Side::WHITE;

  uint16_t NumEmptyFields = W * H;
  uint16_t FirstEmptyField;

  int16_t WhiteCaptured = 0;
  int16_t BlackCaptured = 0;
  
private:
  void Place(uint16_t index, Side side);
  uint16_t Root(uint16_t index);
  uint16_t AreaToRoot(uint16_t index);
  uint16_t AreaAndRoot(uint16_t& index);


  // Returns true iff we make a closed loop.
  // - neighbourChains will contain directions to neighbour chains of the same color. 
  //   Different entries will point to chain with different root.
  // 
  template<Side>
  bool MarkSurroundedAreas(uint16_t index, bool capture, static_vector<uint8_t, 4>& neighbourChains);
  template<Side, FillMode>
  void FloodFillFrom(uint16_t index);
  // Returns true iff an enemy piece was encountered while surrounding (the caller should call CaptureBy).
  template<Side>
  bool SurroundBy(uint16_t index);
  // Returns true iff surrounding of the neighbours should be continued.
  template<Side>
  bool SurroundPieceBy(uint16_t index, bool* capture);
  template<Side>
  void CaptureBy(uint16_t index);
  // Returns true iff capturing of the neighbours should be continues.
  template<Side>
  bool CapturePieceBy(uint16_t index, bool* capture);
  template<Side>
  void ClearSurrounding(uint16_t index);
  // Returns true iff the neighbours should have it's surrounded state cleared.
  template<Side>
  bool ClearPieceSurrounding(uint16_t index);

  template<Side>
  void MergeAdjacentChains(uint16_t index, const static_vector<uint8_t, 4>& neighbourChains);
  template<Side>
  void MakeRoot(uint16_t index, uint16_t currentParent);
  template<Side>
  void ReParentPiecesTo(uint16_t targetParent, uint16_t currentIndex, int16_t area);

  void RemoveEmptyField(uint16_t index);

  static constexpr std::array<int16_t, 8> indicesAround{-(W+2)-1, -(W+2), -(W+2)+1, 1, (W+2)+1, W+2, (W+2)-1, -1};
  static constexpr std::array<int16_t, 8> dxs{-1, 0, 1, 1, 1, 0, -1, -1};
  static constexpr std::array<int16_t, 8> dys{-1, -1, -1, 0, 1, 1, 1, 0};
};

using Board_10_10 = Board<10, 10>;

template<uint16_t W, uint16_t H>
Board<W, H>::Board() 
  : FirstEmptyField(CoordsToIndex(0, 0)) {
  Reset();
}

template<uint16_t W, uint16_t H>
void  Board<W, H>::Reset() {
  std::fill(Chains.begin(), Chains.end(), 0);
  std::fill(Area.begin(), Area.end(), 0);
  for (uint16_t x = 0; x < W; ++x) {
    const uint16_t nextX = (x < W - 1) ? x + 1 : 0;
    const uint16_t prevX = (x > 0) ? x - 1 : W - 1;
    for (uint16_t y = 0; y < H; ++y) {
      const uint16_t nextY = (x < W - 1) ? y : ((y < H - 1) ? y + 1 : 0);
      const uint16_t prevY = (x > 0) ? y : (y > 0 ? y - 1 : (H - 1));
      Chains[CoordsToIndex(x, y)] = CoordsToIndex(nextX, nextY);
      Area[CoordsToIndex(x, y)] = (int16_t)CoordsToIndex(prevX, prevY);
    }
  }
  SideToMove = Side::WHITE;

  NumEmptyFields = W * H;
  FirstEmptyField = CoordsToIndex(0, 0);
  WhiteCaptured = 0;
  BlackCaptured = 0;
}

template<uint16_t W, uint16_t H>
bool Board<W, H>::CanBePlayed(uint16_t x, uint16_t y) const {
  return CanBePlayed(CoordsToIndex(x, y));
}

template<uint16_t W, uint16_t H>
bool Board<W, H>::CanBePlayed(uint16_t index) const {
  if (!IndexOnBoard(index)) {
    return false;
  }
  if (Pieces[index] != FieldState::EMPTY &&
      Pieces[index] != FieldState::EMPTY_SURROUNDED_BY_WHITE && 
      Pieces[index] != FieldState::EMPTY_SURROUNDED_BY_BLACK) {
    return false;
  }
  return true;
}


template<uint16_t W, uint16_t H>
void Board<W, H>::Place(uint16_t index, Side side) {
  X_ASSERT(IndexOnBoard(index));
  X_ASSERT(Pieces[index] == FieldState::EMPTY || Pieces[index] == FieldState::EMPTY_SURROUNDED_BY_WHITE || Pieces[index] == FieldState::EMPTY_SURROUNDED_BY_BLACK);
  RemoveEmptyField(index);
  const bool isPlayOnSurrounded = Pieces[index] == ((side == Side::BLACK) ? FieldState::EMPTY_SURROUNDED_BY_WHITE : FieldState::EMPTY_SURROUNDED_BY_BLACK);
  Pieces[index] = SideToFieldState(side);
  Chains[index] = index;
  Area[index] = 0;
  static_vector<uint8_t, 4> neighbourChains;
  if (side == Side::BLACK) {
    const bool closedChain = MarkSurroundedAreas<Side::BLACK>(index, isPlayOnSurrounded, neighbourChains);
    if (isPlayOnSurrounded && !closedChain) {
      CaptureBy<Side::WHITE>(index);
    } else {
      MergeAdjacentChains<Side::BLACK>(index, neighbourChains);
    }
  } else {
    const bool closedChain = MarkSurroundedAreas<Side::WHITE>(index, isPlayOnSurrounded, neighbourChains);
    if (isPlayOnSurrounded && !closedChain) {
      CaptureBy<Side::BLACK>(index);
    } else {
      MergeAdjacentChains<Side::WHITE>(index, neighbourChains);
    }
  }
}

template<uint16_t W, uint16_t H>
void Board<W, H>::Place(uint16_t index) {
  Place(index, SideToMove);
  SideToMove = OppositeSide(SideToMove);
}

template<uint16_t W, uint16_t H>
void Board<W, H>::Place(uint16_t x, uint16_t y) {
  Place(CoordsToIndex(x, y));
}

template<uint16_t W, uint16_t H>
void Board<W, H>::Place(uint16_t x, uint16_t y, Side side) {
  Place(CoordsToIndex(x, y), side);
}

template<uint16_t W, uint16_t H>
void Board<W, H>::MakeCanonical() {
  if (SideToMove == Side::WHITE) {
    return;
  }
  for (uint16_t index = 0; index < Pieces.size(); ++index) {
    const FieldState state = Pieces[index];
    const FieldState oppositeState = OppositeState(state);
    Pieces[index] = oppositeState;
  }
  std::swap(WhiteCaptured, BlackCaptured);
  SideToMove = Side::WHITE;
}

template<uint16_t W, uint16_t H>
std::vector<uint16_t> Board<W, H>::GetEmptyFields() const {
  std::vector<uint16_t> emptyFields(NumEmptyFields);
  if (NumEmptyFields > 0) {
    size_t curI = 0;
    uint16_t cur = FirstEmptyField;
    do {
      X_ASSERT(curI < NumEmptyFields);
      emptyFields[curI] = cur;
      cur = Chains[cur];
      ++curI;
    } while (cur != FirstEmptyField);
    X_ASSERT(curI == NumEmptyFields);
  }
  X_ASSERT(NumEmptyFields == emptyFields.size());
  return emptyFields;
}

template<uint16_t W, uint16_t H>
void Board<W, H>::GetEmptyFields(std::vector<uint16_t>* emptyFields) const {
  X_ASSERT(emptyFields->size() == NumEmptyFields);
  if (NumEmptyFields > 0) {
    size_t curI = 0;
    uint16_t cur = FirstEmptyField;
    do {
      X_ASSERT(curI < NumEmptyFields);
      (*emptyFields)[curI] = cur;
      cur = Chains[cur];
      ++curI;
    } while (cur != FirstEmptyField);
    X_ASSERT(curI == NumEmptyFields);
  }
}

template<uint16_t W, uint16_t H>
uint16_t Board<W, H>::GetNthEmptyField(uint16_t n) const {
  X_ASSERT(NumEmptyFields > 0 && n < NumEmptyFields);
  uint16_t result = FirstEmptyField;
  while (n > 0) {
    result = Chains[result];
    --n;
  }
  return result;
}

template<uint16_t W, uint16_t H>
std::vector<std::vector<int>> Board<W, H>::GetObservationVectors() const {
  std::vector<std::vector<int>> vectors((size_t)(FieldState::NUM_STATES));
  for (FieldState s = (FieldState)0; s != FieldState::NUM_STATES; s = (FieldState)((uint8_t)s+1)) {
    vectors[(uint8_t)s].reserve(AllBoardIndices.size());
    for (uint16_t index : AllBoardIndices) {
      vectors[(uint8_t)s].push_back((Pieces[index] == s) ? 1 : 0);
    }
  }
  return vectors;
}

template<uint16_t W, uint16_t H>
void Board<W, H>::RemoveEmptyField(uint16_t index) {
  X_ASSERT(IndexOnBoard(index));
  X_ASSERT(NumEmptyFields > 0);
  --NumEmptyFields;
  const uint16_t nextEmptyField = Chains[index];
  X_ASSERT(IndexOnBoard(nextEmptyField));
  const uint16_t prevEmptyField = (uint16_t)Area[index];
  X_ASSERT(IndexOnBoard(prevEmptyField));
  X_ASSERT(NumEmptyFields == 0 || FieldState::EMPTY == Pieces[nextEmptyField] || FieldState::EMPTY_SURROUNDED_BY_BLACK == Pieces[nextEmptyField] || FieldState::EMPTY_SURROUNDED_BY_WHITE == Pieces[nextEmptyField]);
  X_ASSERT(NumEmptyFields == 0 || FieldState::EMPTY == Pieces[prevEmptyField] || FieldState::EMPTY_SURROUNDED_BY_BLACK == Pieces[prevEmptyField] || FieldState::EMPTY_SURROUNDED_BY_WHITE == Pieces[prevEmptyField]);
  Area[nextEmptyField] = (int16_t)prevEmptyField;
  Chains[prevEmptyField] = nextEmptyField;
  if (index == FirstEmptyField) {
    FirstEmptyField = nextEmptyField;
  }
}

template<uint16_t W, uint16_t H>
template<Side side>
bool Board<W, H>::MarkSurroundedAreas(uint16_t index, bool isPlayOnSurrounded, static_vector<uint8_t, 4>& neighbourChains) {
  X_ASSERT(IndexOnBoard(index));
  static_vector<uint8_t, 4> separatedGroupBegins;
  static_vector<uint8_t, 4> separatedGroupEnds;
  int8_t requiredSeparation = 0;
  for (uint8_t i = 0; i < 8; ++i) {
    const uint16_t neighbourIndex = index + indicesAround[i];
    const FieldState neighbourPiece = Pieces[neighbourIndex];
    if (!PieceIsOfSide<side>(neighbourPiece)) {
      --requiredSeparation;
      continue;
    }
    if (requiredSeparation > 0) {
      separatedGroupEnds.back() = i;
      requiredSeparation = 1 + (i%2);
      continue;
    }
    separatedGroupBegins.push_back(i);
    separatedGroupEnds.push_back(i);
    requiredSeparation = 1 + (i%2);
  }
  if (separatedGroupBegins.size() <= 1) {
    return false;
  }
  
  if (separatedGroupEnds.back() == 7 && separatedGroupBegins.front() <= 1) {
    separatedGroupBegins.front() = separatedGroupBegins.back();
    separatedGroupBegins.pop_back();
    separatedGroupEnds.pop_back();
  }
  if (separatedGroupBegins.size() <= 1) {
    return false;
  }

  static_vector<uint16_t, 4> roots;
  static_vector<int16_t, 4> areas;
  const int16_t y = (int16_t)IndexToY(index);
  for (uint8_t i : separatedGroupBegins) {
    uint16_t root = index + indicesAround[i];
    const int16_t area = AreaAndRoot(root);
    roots.push_back(root);
    areas.push_back(area + dxs[i]*(y+y+dys[i]));
  }
  bool closedChain = false;
  for (uint8_t i = 0; i < separatedGroupBegins.size(); ++i) {
    neighbourChains.push_back(separatedGroupBegins[i]);
    for (uint8_t j = i+1; j < separatedGroupBegins.size(); ++j) {
      if (roots[i] != roots[j]) {
	continue;
      }
      X_ASSERT(areas[i] != areas[j]);
      closedChain = true;
      if (areas[j] - areas[i] > 0) {
	for (uint8_t k = (separatedGroupEnds[i]+1)%8; k != separatedGroupBegins[j]; k=(k+1)%8) {
	  if (isPlayOnSurrounded) {
	    FloodFillFrom<side, FillMode::CAPTURE>(index + indicesAround[k]);
	  } else {
	    FloodFillFrom<side, FillMode::SURROUND>(index + indicesAround[k]);
	  }
	}
	i = j;
      } else {
	for (uint8_t k = (separatedGroupEnds[j]+1)%8; k != separatedGroupBegins[i]; k=(k+1)%8) {
	  if (isPlayOnSurrounded) {
	    FloodFillFrom<side, FillMode::CAPTURE>(index + indicesAround[k]);
	  } else {
	    FloodFillFrom<side, FillMode::SURROUND>(index + indicesAround[k]);
	  }
	}
	goto loop_end;
      }
    }
  }
 loop_end:
#ifndef NDEBUG
  // check that neighbourChains is actually a set.
  std::set<uint16_t> rootSet;
  for (uint8_t i : neighbourChains) {
    const uint16_t neighbour = index + indicesAround[i];
    const uint16_t root = Root(neighbour);
    auto res = rootSet.insert(root);
    X_ASSERT(res.second);
  }
#endif
  // we made a capture by playing on a surrounded area, mark nearby empty fields
  // as not surrounded anymore.
  if (isPlayOnSurrounded && closedChain) {
    for (uint8_t i : {1, 3, 5, 7}) {
      FloodFillFrom<side, FillMode::EMPTY>(index + indicesAround[i]);
    }
  }
  return closedChain;
}

template<uint16_t W, uint16_t H>
template<Side side>
void Board<W, H>::MergeAdjacentChains(uint16_t index, const static_vector<uint8_t, 4>& neighbourChains) {
  X_ASSERT(IndexOnBoard(index));
  X_ASSERT(Pieces[index] == FieldState::WHITE || Pieces[index] == FieldState::BLACK);
  const uint16_t y = IndexToY(index);
  for (uint8_t i : neighbourChains) {
    const uint16_t neighbourIndex = index + indicesAround[i];
    const int16_t dx = dxs[i], dy = dys[i];
    const int16_t area = dx * (y + y + dy);
    ReParentPiecesTo<side>(index, neighbourIndex, -area);
  }
}

template<uint16_t W, uint16_t H>
template<Side side, FillMode mode>
void Board<W, H>::FloodFillFrom(uint16_t index) {
  if constexpr(mode == FillMode::CAPTURE) {
    CaptureBy<side>(index);
  } else if constexpr(mode == FillMode::EMPTY) {
    ClearSurrounding<OppositeSide(side)>(index);
  } else {
    if (SurroundBy<side>(index)) {
      CaptureBy<side>(index);
    }
  }
}

template<uint16_t W, uint16_t H>
template<Side side>
bool Board<W, H>::SurroundPieceBy(uint16_t index, bool* capture) {
  X_ASSERT(IndexOnBoard(index));
  if constexpr(side == Side::WHITE) {
    switch(Pieces[index]) {
    case FieldState::EMPTY:
      Pieces[index] = FieldState::EMPTY_SURROUNDED_BY_WHITE;
      return true;
    case FieldState::BLACK:
      *capture = true;
      return true;
    case FieldState::EMPTY_SURROUNDED_BY_WHITE: [[fallthrough]];
    case FieldState::WHITE: [[fallthrough]];
    case FieldState::CAPTURED_BLACK: [[fallthrough]];
    case FieldState::EMPTY_CAPTURED_BY_WHITE:
      return false;
    case FieldState::CAPTURED_WHITE:
      X_ASSERT(false);
      return false;
    case FieldState::EMPTY_SURROUNDED_BY_BLACK:
      X_ASSERT(false);
      return false;
    case FieldState::EMPTY_CAPTURED_BY_BLACK:
      X_ASSERT(false);
      return false;
    default:
      X_ASSERT(false);
    }
  } else {
    switch(Pieces[index]) {
    case FieldState::EMPTY:
      Pieces[index] = FieldState::EMPTY_SURROUNDED_BY_BLACK;
      return true;
    case FieldState::WHITE:
      *capture = true;
      return true;
    case FieldState::BLACK: [[fallthrough]];
    case FieldState::CAPTURED_WHITE: [[fallthrough]];
    case FieldState::EMPTY_SURROUNDED_BY_BLACK: [[fallthrough]];
    case FieldState::EMPTY_CAPTURED_BY_BLACK:
      return false;
    case FieldState::CAPTURED_BLACK:
      X_ASSERT(false);
      return false;
    case FieldState::EMPTY_SURROUNDED_BY_WHITE:
      X_ASSERT(false);
      return false;
    case FieldState::EMPTY_CAPTURED_BY_WHITE:
      X_ASSERT(false);
      return false;
    default:
      X_ASSERT(false);
    }
  }
  return false;
}

template<uint16_t W, uint16_t H>
template<Side side>
bool Board<W, H>::SurroundBy(uint16_t index) {
  bool capture = false;
  static_vector<uint16_t, (W-2)*(H-2)+1> stack;
  if (SurroundPieceBy<side>(index, &capture)) {
    if (capture) {
      return true;
    }
    stack.push_back(index);
  }
  while (!stack.empty()) {
    const uint16_t index = stack.back();
    stack.pop_back();
    X_ASSERT(IndexOnBoard(index));
    X_ASSERT(IndexToX(index) != 0);
    X_ASSERT(IndexToX(index) != (W + 1));
    X_ASSERT(IndexToY(index) != 0);
    X_ASSERT(IndexToY(index) != (H + 1));
    for (uint16_t neighbour : {index - 1, index + 1, index - (W+2), index + (W+2)}) {
      if (SurroundPieceBy<side>(neighbour, &capture)) {
	if (capture) {
	  return true;
	}
	stack.push_back(neighbour);
      }
    }
  }
  return false;

}

template<uint16_t W, uint16_t H>
template<Side side>
bool Board<W, H>::CapturePieceBy(uint16_t index, bool* capture) {
  X_ASSERT(IndexOnBoard(index));
  if constexpr(side == Side::WHITE) {
    switch(Pieces[index]) {
    case FieldState::EMPTY: [[fallthrough]];
    case FieldState::EMPTY_SURROUNDED_BY_WHITE: [[fallthrough]];
    case FieldState::EMPTY_SURROUNDED_BY_BLACK:
      Pieces[index] = FieldState::EMPTY_CAPTURED_BY_WHITE;
      RemoveEmptyField(index);
      return true;
    case FieldState::EMPTY_CAPTURED_BY_BLACK:
      Pieces[index] = FieldState::EMPTY_CAPTURED_BY_WHITE;
      return true;
    case FieldState::BLACK:
      Pieces[index] = FieldState::CAPTURED_BLACK;
      ++BlackCaptured;
      *capture = true;
      return true;
    case FieldState::CAPTURED_WHITE:
      Pieces[index] = FieldState::WHITE;
      X_ASSERT(WhiteCaptured > 0);
      --WhiteCaptured;
      return true;
    case FieldState::WHITE: [[fallthrough]];
    case FieldState::CAPTURED_BLACK: [[fallthrough]];
    case FieldState::EMPTY_CAPTURED_BY_WHITE:
      return false;
    default:
      X_ASSERT(false);
    }
  } else {
    switch(Pieces[index]) {
    case FieldState::EMPTY: [[fallthrough]];
    case FieldState::EMPTY_SURROUNDED_BY_WHITE: [[fallthrough]];
    case FieldState::EMPTY_SURROUNDED_BY_BLACK:
      Pieces[index] = FieldState::EMPTY_CAPTURED_BY_BLACK;
      RemoveEmptyField(index);
      return true;
    case FieldState::EMPTY_CAPTURED_BY_WHITE:
      Pieces[index] = FieldState::EMPTY_CAPTURED_BY_BLACK;
      return true;
    case FieldState::WHITE:
      Pieces[index] = FieldState::CAPTURED_WHITE;
      ++WhiteCaptured;
      *capture = true;
      return true;
    case FieldState::CAPTURED_BLACK:
      Pieces[index] = FieldState::BLACK;
      X_ASSERT(BlackCaptured > 0);
      --BlackCaptured;
      return true;
    case FieldState::BLACK: [[fallthrough]];
    case FieldState::CAPTURED_WHITE: [[fallthrough]];
    case FieldState::EMPTY_CAPTURED_BY_BLACK:
      return false;
    default:
      X_ASSERT(false);
    }
  }
  return false;
}

template<uint16_t W, uint16_t H>
template<Side side>
void Board<W, H>::CaptureBy(uint16_t index) {
  static_vector<uint16_t, W*H/2> captured;
  static_vector<uint16_t, (W-2)*(H-2)+1> stack;
  bool capture = false;
  if (CapturePieceBy<side>(index, &capture)) {
    stack.push_back(index);
    if (capture) {
      captured.push_back(index);
    }
  }
  while (!stack.empty()) {
    const uint16_t index = stack.back();
    stack.pop_back();
    X_ASSERT(IndexOnBoard(index));
    X_ASSERT(IndexToX(index) != 0);
    X_ASSERT(IndexToX(index) != (W + 1));
    X_ASSERT(IndexToY(index) != 0);
    X_ASSERT(IndexToY(index) != (H + 1));
    for (uint16_t neighbour : {index - 1, index + 1, index - (W+2), index + (W+2)}) {
      capture = false;
      if (CapturePieceBy<side>(neighbour, &capture)) {
	stack.push_back(neighbour);
	if (capture) {
	  captured.push_back(neighbour);
	}
      }
    }
  }
  //  if (capture) {
  for (uint16_t index : captured) {
    if constexpr(side == Side::WHITE) {
      X_ASSERT(Pieces[index] == FieldState::CAPTURED_BLACK);
    } else {
      X_ASSERT(Pieces[index] == FieldState::CAPTURED_WHITE);
    }
    // Check neighbours in cardinal direction to see if they break the opposite chain.
    for (uint8_t i = 1; i < 8; i += 2) {
      const uint16_t neighbourIndex = index + indicesAround[i];
      const FieldState neighbourState = Pieces[neighbourIndex];
      const uint16_t nextNeighbourIndex = index + indicesAround[(i+1)%8];
      const FieldState nextNeighbourState = Pieces[nextNeighbourIndex];
      const uint16_t nextNeighbourParent = Chains[nextNeighbourIndex];
      const uint16_t nextNextNeighbourIndex = index + indicesAround[(i+2)%8];
      const FieldState nextNextNeighbourState = Pieces[nextNextNeighbourIndex];
      if constexpr (side == Side::WHITE) {
        if (nextNeighbourParent == index &&
	    neighbourState == FieldState::WHITE &&
	    nextNeighbourState == FieldState::BLACK && nextNextNeighbourState == FieldState::WHITE) {
	  MakeRoot<Side::BLACK>(nextNeighbourIndex, nextNeighbourParent);
	}
      } else {
	if (nextNeighbourParent == index && neighbourState == FieldState::BLACK &&
	    nextNeighbourState == FieldState::WHITE && nextNextNeighbourState == FieldState::BLACK) {
	  MakeRoot<Side::WHITE>(nextNeighbourIndex, nextNeighbourParent);
	}
      }
    }
  }
}


template<uint16_t W, uint16_t H>
template<Side side>
bool Board<W, H>::ClearPieceSurrounding(uint16_t index) {
  X_ASSERT(IndexOnBoard(index));
  if constexpr(side == Side::BLACK) {
    switch(Pieces[index]) {
    case FieldState::EMPTY_SURROUNDED_BY_BLACK:
      Pieces[index] = FieldState::EMPTY;
      return true;
    case FieldState::EMPTY: [[fallthrough]];
    case FieldState::WHITE: [[fallthrough]];
    case FieldState::BLACK: [[fallthrough]];
    case FieldState::CAPTURED_BLACK: [[fallthrough]];
    case FieldState::EMPTY_CAPTURED_BY_WHITE:
      return false;
    case FieldState::CAPTURED_WHITE:
      X_ASSERT(false);
      return false;
    case FieldState::EMPTY_SURROUNDED_BY_WHITE:
      X_ASSERT(false);
      return false;
    case FieldState::EMPTY_CAPTURED_BY_BLACK:
      X_ASSERT(false);
      return false;
    default:
      X_ASSERT(false);
    }
  } else {
    switch(Pieces[index]) {
    case FieldState::EMPTY_SURROUNDED_BY_WHITE:
      Pieces[index] = FieldState::EMPTY;
      return true;
    case FieldState::EMPTY: [[fallthrough]];
    case FieldState::WHITE: [[fallthrough]];
    case FieldState::BLACK: [[fallthrough]];
    case FieldState::CAPTURED_WHITE: [[fallthrough]];
    case FieldState::EMPTY_CAPTURED_BY_BLACK:
      return false;
    case FieldState::CAPTURED_BLACK:
      X_ASSERT(false);
      return false;
    case FieldState::EMPTY_SURROUNDED_BY_BLACK:
      X_ASSERT(false);
      return false;
    case FieldState::EMPTY_CAPTURED_BY_WHITE:
      X_ASSERT(false);
      return false;
    default:
      X_ASSERT(false);
    }
  }
  return false;
}

template<uint16_t W, uint16_t H>
template<Side side>
void Board<W, H>::ClearSurrounding(uint16_t index) {
  static_vector<uint16_t, (W-2)*(H-2)+1> stack;
  if (ClearPieceSurrounding<side>(index)) {
    stack.push_back(index);
  }
  while (!stack.empty()) {
    uint16_t index = stack.back();
    stack.pop_back();
    X_ASSERT(IndexToX(index) != 0);
    X_ASSERT(IndexToX(index) != (W + 1));
    X_ASSERT(IndexToY(index) != 0);
    X_ASSERT(IndexToY(index) != (H + 1));
    for (uint16_t neighbour : {index - 1, index + 1, index - (W+2), index + (W+2)}) {
      if (ClearPieceSurrounding<side>(neighbour)) {
	stack.push_back(neighbour);
      }
    }
  }
}

template<uint16_t W, uint16_t H>
template<Side side>
void Board<W, H>::MakeRoot(uint16_t index, uint16_t currentParent) {
  X_ASSERT(Pieces[index] == FieldState::BLACK || Pieces[index] == FieldState::WHITE);
  X_ASSERT(currentParent != index);
  const int16_t currentArea = Area[index];
  Chains[index] = index;
  Area[index] = 0;

  ReParentPiecesTo<side>(index, currentParent, -currentArea);
}

template<uint16_t W, uint16_t H>
template<Side side>
void Board<W, H>::ReParentPiecesTo(uint16_t targetParent, uint16_t currentIndex, int16_t area) {
  while (true) {
    if constexpr(side == Side::WHITE) {
      X_ASSERT(Pieces[currentIndex] == FieldState::WHITE || Pieces[currentIndex] == FieldState::CAPTURED_WHITE);
    } else {
      X_ASSERT(Pieces[currentIndex] == FieldState::BLACK || Pieces[currentIndex] == FieldState::CAPTURED_BLACK);
    }
    // don't reparent captured pieces
    if (!PieceIsOfSide<side>(Pieces[currentIndex])) {
      return;
    }
    X_ASSERT(Pieces[currentIndex] == FieldState::BLACK || Pieces[currentIndex] == FieldState::WHITE);
    X_ASSERT(targetParent != currentIndex);
    const uint16_t currentParent = Chains[currentIndex];
    X_ASSERT(currentParent != targetParent);
    Chains[currentIndex] = targetParent;
    const int16_t currentArea = Area[currentIndex];
    Area[currentIndex] = area;
    // We've reached root we can stop now.
    if (currentParent == currentIndex) {
      return;
    }
    targetParent = currentIndex;
    currentIndex = currentParent;
    area = -currentArea;
  }
}

template<uint16_t W, uint16_t H>
FieldState Board<W, H>::PieceAt(uint16_t x, uint16_t y) const {
  return Pieces[CoordsToIndex(x, y)];
}

template<uint16_t W, uint16_t H>
constexpr uint16_t Board<W, H>::CoordsToIndex(uint16_t x, uint16_t y) {
  X_ASSERT(x < W);
  X_ASSERT(y < H);
  return (y+1) * (W+2) + x+1;
}

template<uint16_t W, uint16_t H>
constexpr void Board<W, H>::IndexToCoords(uint16_t index, uint16_t& x, uint16_t& y) {
  x = IndexToX(index);
  y = IndexToY(index);
}

template<uint16_t W, uint16_t H>
std::string Board<W, H>::IndexToString(uint16_t index) {
  uint16_t x, y;
  IndexToCoords(index, x, y);
  return "(" + std::to_string(x) + "," + std::to_string(y) + ")";
}

template<uint16_t W, uint16_t H>
constexpr uint16_t Board<W, H>::IndexToX(uint16_t index) {
  return index % (W + 2) - 1;
}

template<uint16_t W, uint16_t H>
constexpr uint16_t Board<W, H>::IndexToY(uint16_t index) {
  return index / (W + 2) - 1;
}

template<uint16_t W, uint16_t H>
constexpr bool Board<W, H>::IndexOnBoard(uint16_t index) {
  uint16_t x, y;
  IndexToCoords(index, x, y);
  return x >= 0 && x < W && y >= 0 && y < H;
}
template<uint16_t W, uint16_t H>
constexpr std::array<uint16_t, W*H> Board<W, H>::GenerateAllBoardIndices() {
  std::array<uint16_t, W*H> indices{};
  uint16_t ix = 0;
  for (uint16_t y = 0; y < H; ++y) {
    for (uint16_t x = 0; x < W; ++x) {
      indices[ix] = CoordsToIndex(x, y);
      ++ix;
    }
  }
  return indices;
}
  /*template<uint16_t W, uint16_t H>
int16_t Board<W, H>::AreaUnder(uint16_t from, uint16_t to) {
  uint16_t x0 = from % (W+2), x1 = to % (W+2);
  uint16_t y1_plus_y2 = (from+to)/(W+2) -((x0+x1)>=W+2) - 2;
  int16_t dx = x1 - x0;
  return dx * y1_plus_y2;
  }*/

template<uint16_t W, uint16_t H>
constexpr std::array<int16_t, 8> Board<W, H>::indicesAround;
template<uint16_t W, uint16_t H>
constexpr std::array<int16_t, 8> Board<W, H>::dxs;
template<uint16_t W, uint16_t H>
constexpr std::array<int16_t, 8> Board<W, H>::dys;

  //template<uint16_t W, uint16_t H>
//std::vector<std::vector<uint64_t>> Board<W, H>::Hashes;

/*template<uint16_t W, uint16_t H>
template<class Rand>
void Board<W, H>::InitializeHashes(Rand& rand) {
  X_ASSERT(Hashes.empty());
  Hashes.resize((W+2)*(H+2), std::vector<uint64_t>((size_t)FieldState::NUM_STATES, (uint64_t)0));
  //  std::seed_seq seq{1,2,3,4,5};  // TODO: check what seed is good
  //  std::mt19937_64 r(seq);
  //  pcg64 r;//(777777777);
  static_assert(sizeof(typename Rand::result_type) >= sizeof(uint64_t));
  for (uint16_t x = 0; x < W; ++x) {
    for (uint16_t y = 0; y < H; ++y) {
      for (uint8_t f = 0; f < (uint8_t)(FieldState::NUM_STATES); ++f) {
	X_ASSERT(CoordsToIndex(x, y) < Hashes.size());
	Hashes[CoordsToIndex(x, y)][f] = (uint64_t)rand();
      }
    }
  }
  }*/

template<uint16_t W, uint16_t H>
int8_t Board<W, H>::Result() const {
  int8_t whiteResult = BlackCaptured == WhiteCaptured ? 0 : (BlackCaptured > WhiteCaptured ? 1 : -1);
  switch (SideToMove) {
  case Side::WHITE: 
    return whiteResult;
  case Side::BLACK:
    return -whiteResult;
  default:
    X_ASSERT(false);
    return 0;
  }
}

template<uint16_t W, uint16_t H>
uint16_t Board<W, H>::Root(uint16_t index) {
  size_t dist = 0;
  while (true) {
    const uint16_t parent = Chains[index];
    if (parent == index) {
      return index;
    }
    index = parent;
    ++dist;
  }
}

 template<uint16_t W, uint16_t H>
 uint16_t Board<W, H>::AreaToRoot(uint16_t index) {
   uint16_t area = 0;
   while (true) {
     const uint16_t parent = Chains[index];
     if (parent == index) {
       return area;
     }
     area += Area[index];
     index = parent;
   }
 }

 template<uint16_t W, uint16_t H>
 uint16_t Board<W, H>::AreaAndRoot(uint16_t& index) {
   uint16_t area = 0;
#ifndef NDEBUG
   uint16_t length = 0;
#endif
   while (true) {
     const uint16_t parent = Chains[index];
     if (parent == index) {
       return area;
     }
     area += Area[index];
     index = parent;
#ifndef NDEBUG
     length++;
     X_ASSERT(length < W*H);
#endif
   }
 }

}
