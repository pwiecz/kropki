#include <iostream>
#include <random>

#include "pcg_random.hpp"

//#include "kropki.h"
#include "kropki_parent.h"
//#include "kropki_debug.h"


using kropki_parent::Board;

int main() {
  constexpr uint32_t W = 32, H = 16;
  pcg64 gen;
  //  Board<W, H>::InitializeHashes(gen);

  //  std::seed_seq seq{time(0)};  // TODO: check what seed is good
  //  std::seed_seq seq{2, 3, 4, 5, 6};
  //  std::mt19937_64 gen(seq);
  //  pcg_extras::seed_seq_from<std::random_device> seed_source;

  //  while (true) {
  for (int i = 0; i < 100000; ++i) {
    //    std::cout << "\n\nNew game:\n";
    Board<W, H> board;
    while (board.NumEmptyFields > 0) {
      std::uniform_int_distribution<> dis(0, board.NumEmptyFields-1);
      uint16_t moveIx = static_cast<uint16_t>(dis(gen));
      uint16_t move = board.GetNthEmptyField(moveIx);
      //      std::cerr << (board.SideToMove == Side::WHITE ? 'o' : 'x')
      //		<< Board<W,H>::IndexToString(move) << "\n";
      //      try {
      board.Place(move);
      //      std::cerr << board.BoardToString();
      //      } catch (AssertionFailedException const& e) {
      //	std::cerr << e.Expr << " in " << e.File << ":" << e.Line << " " << e.Func << "\n";
      //	return 1;
      //      }
    }
  }
}
