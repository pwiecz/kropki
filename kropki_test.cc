#include <iostream>

#include <cassert>

#include "catch.hpp"

#include "kropki.h"
#include "kropki_debug.h"


using kropki::Board;

TEST_CASE("EmptyBoard") {
  constexpr uint16_t W = 64, H = 32;
  Board<W, H> emptyBoard;
}

TEST_CASE("CoordIndices") {
  constexpr uint16_t W = 64, H = 32;
  for (uint16_t x = -1; x <= W; ++x) {
    for (uint16_t y = -1; y <= H; ++y) {
      const uint16_t index = Board<W, H>::CoordsToIndex(x, y);
      uint16_t xo, yo;
      Board<W, H>::IndexToCoords(index, xo, yo);
      REQUIRE(x == xo);
      REQUIRE(y == yo);
      uint16_t yo2 = Board<W, H>::IndexToY(index);
      REQUIRE(y == yo2);
    }
  }
}

TEST_CASE("CannotPlaceTwiceOnSameField") {
  constexpr uint16_t W = 32, H = 16;
  Board<W, H> board;
  for (uint16_t x = 0; x < W; ++x) {
    for (uint16_t y = 0; y < H; ++y) {
      REQUIRE(board.CanBePlayed(x, y));
      board.Place(x, y, Side::BLACK);
      REQUIRE(!board.CanBePlayed(x, y));
    }
  }
}

TEST_CASE("SimpleSurround") {
  constexpr uint16_t W = 32, H = 16;
  Board<W, H> board;
  board.Place(10, 10, Side::WHITE);
  board.Place(10, 9, Side::BLACK);
  board.Place(9, 10, Side::BLACK);
  board.Place(10, 11, Side::BLACK);
  board.Place(11, 10, Side::BLACK);
  REQUIRE(1 == board.WhiteCaptured);
  REQUIRE(FieldState::CAPTURED_WHITE == board.PieceAt(10, 10));
  REQUIRE(FieldState::BLACK == board.PieceAt(10, 9));
  REQUIRE(FieldState::BLACK == board.PieceAt(9, 10));
  REQUIRE(FieldState::BLACK == board.PieceAt(10, 11));
  REQUIRE(FieldState::BLACK == board.PieceAt(11, 10));
}

TEST_CASE("SimpleSurroundOppositeOrder") {
  constexpr uint16_t W = 32, H = 16;
  Board<W, H> board;
  board.Place(10, 10, Side::WHITE);
  board.Place(11, 10, Side::BLACK);
  board.Place(10, 11, Side::BLACK);
  board.Place(9, 10, Side::BLACK);
  board.Place(10, 9, Side::BLACK);
  REQUIRE(1 == board.WhiteCaptured);
  REQUIRE(FieldState::CAPTURED_WHITE == board.PieceAt(10, 10));
  REQUIRE(FieldState::BLACK == board.PieceAt(10, 9));
  REQUIRE(FieldState::BLACK == board.PieceAt(9, 10));
  REQUIRE(FieldState::BLACK == board.PieceAt(10, 11));
  REQUIRE(FieldState::BLACK == board.PieceAt(11, 10));
}

TEST_CASE("SimpleBreakChain") {
  constexpr uint16_t W = 32, H = 16;
  const uint16_t index1010 = Board<W, H>::CoordsToIndex(10, 10);
  const uint16_t index99 = Board<W, H>::CoordsToIndex(9, 9);
  const uint16_t index88 = Board<W, H>::CoordsToIndex(8, 8);
  Board<W, H> board;
  board.Place(10, 10, Side::WHITE);
  board.Place(9, 9, Side::WHITE);
  board.Place(8, 8, Side::WHITE);
  REQUIRE(index1010 == board.Chains[index99]);
  REQUIRE(index1010 == board.Chains[index88]);
  board.Place(11, 10, Side::BLACK);
  board.Place(10, 11, Side::BLACK);
  board.Place(9, 10, Side::BLACK);
  board.Place(10, 9, Side::BLACK);
  REQUIRE(1 == board.WhiteCaptured);
  REQUIRE(FieldState::CAPTURED_WHITE == board.PieceAt(10, 10));
  REQUIRE(index99 == board.Chains[index99]);
  REQUIRE(2 == board.Winding[index99]);
}

TEST_CASE("MergingChains_Regression1") {
  constexpr uint16_t W = 32, H = 16;
  Board<W, H> board;
  board.Place(0, 0, Side::WHITE);
  board.Place(1, 1, Side::WHITE);
  //  board.Place(1, 2, Side::WHITE);
  board.Place(0, 1, Side::WHITE);
}

TEST_CASE("MergingChains_Regression2") {
  constexpr uint16_t W = 32, H = 16;
  Board<W, H> board;
  board.Place(1, 2, Side::WHITE);
  board.Place(0, 0, Side::WHITE);
  board.Place(2, 0, Side::WHITE);
  board.Place(1, 1, Side::WHITE);
  board.Place(0, 1, Side::WHITE);
}

TEST_CASE("MergingChains_Regression3") {
  constexpr uint16_t W = 32, H = 16;
  Board<W, H> board;
  board.Place(1, 0, Side::WHITE);
  board.Place(0, 1, Side::WHITE);
  board.Place(1, 1, Side::WHITE);
  board.Place(1, 1, Side::WHITE);
}
