#pragma once

#include "kropki.h"
#include "kropki_debug.h"

#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <unordered_map>

const float C = std::sqrt(2.f);

class Node {
public:
  Node(uint64_t Hash, std::vector<uint16_t> actions);

  void Update(int8_t res);
  size_t BestIndex() const;

  uint64_t const Hash;
  //  Node* Parent;
  const std::vector<uint16_t> Actions;
  std::vector<Node*> Children;
  float Wins = 0.f;
  uint32_t Simulations = 0;
  //  uint32_t TotalSimulations = 0;
  uint32_t NumRefs = 0;
};

Node::Node(uint64_t hash, std::vector<uint16_t> actions)
  : Hash(hash)
    //  , Parent(parent) 
  , Actions(std::move(actions))
  , Children(Actions.size(), nullptr) {}

void Node::Update(int8_t res) {
  if (res > 0) {
    ++Wins;
  } else if (res == 0) {
    Wins += 0.01f;
  }
  ++Simulations;
  //  X_ASSERT(Parent != nullptr);
  //  ++(Parent->TotalSimulations);
}

size_t Node::BestIndex() const {
  uint32_t totalSimulations = 0;
  for (size_t i = 0; i < Children.size(); ++i) {
    Node const * const child = Children[i];
    if (child == nullptr) {
      return i;
    }
    totalSimulations += Children[i]->Simulations;
  }
  const float logN = (totalSimulations > 0) ? std::log((float)totalSimulations) : 1.f;
  float bestScore = std::numeric_limits<float>::lowest();
  size_t bestIndex = 0;
  for (size_t i = 0; i < Children.size(); ++i) {
    Node const * const child = Children[i];
    const float score = child->Wins / child->Simulations + C * std::sqrt(logN / child->Simulations);
    X_ASSERT(score > std::numeric_limits<float>::lowest());
    if (score > bestScore) {
      bestScore = score;
      bestIndex = i;
    }
  }
  X_ASSERT(bestScore > std::numeric_limits<float>::lowest());
  return bestIndex;
}

template<class T, class RandomGen>
std::vector<T> RandomPermutation(std::vector<T> v, RandomGen* rand) {
  std::shuffle(v.begin(), v.end(), *rand);
  return v;
}

template<class GameState, class RandomGen>
class MCTS {
public:
  MCTS(GameState* state, RandomGen* rand);
  ~MCTS() {
    //    std::cerr << "Hash size: " << Nodes.size();
  }
  void Update();

  uint16_t BestMove() const;
  void ApplyBestMove();

private:
  int8_t PlayFrom(Node* node, GameState* state);
  void DeleteTree(Node* node);
  GameState* const RootState;
  RandomGen* const Random;
  Node* Root;
  std::unordered_map<uint64_t, std::unique_ptr<Node>> Nodes;
};

template<class GameState, class RandomGen>
MCTS<GameState, RandomGen>::MCTS(GameState* state, RandomGen* rand)
  : RootState(state)
  , Random(rand)
  , Root(new Node(state->ZobristHash, RandomPermutation(RootState->GetEmptyFields(), rand))) {}

template<class GameState, class RandomGen>
void MCTS<GameState, RandomGen>::Update() {
  GameState currentState = *RootState;
  PlayFrom(Root, &currentState);
}

template<class GameState, class RandomGen>
uint16_t MCTS<GameState, RandomGen>::BestMove() const {
  uint32_t bestNumVisits = 0;
  //uint16_t bestMove = 0;
  size_t bestMoveI = 0;
  for (size_t i = 0; i < Root->Children.size(); ++i) {
    Node* child = Root->Children[i];
    if (child == nullptr) {
      X_ASSERT(false);
      continue;
    }
    if (child->Simulations > bestNumVisits) {
      bestNumVisits = child->Simulations;
      bestMoveI = i;//Root->Actions[i];
      //      bestMoveI = i;
    }
  }
  X_ASSERT(bestNumVisits > 0);
  //  std::cerr << "Best move(" << bestMoveI <<") visits: " << bestNumVisits << " wins: " <<Root->Children[bestMoveI]->Wins << "\n";
  return bestMoveI;
}

template<class GameState, class RandomGen>
int8_t MCTS<GameState, RandomGen>::PlayFrom(Node* node, GameState* state) {
  if (state->NumEmptyFields == 0) {
    const int8_t res = state->Result();
    node->Update(res);
    return res;
  }
  X_ASSERT(node->Children.size() > 0);
  const size_t bestMove = node->BestIndex();
  state->Place(node->Actions[bestMove]);
  //  std::cerr << "best rollout wins: " << node->Wins[bestMove] << "\n";
  Node* nextNode = node->Children[bestMove];
  if (nextNode == nullptr) {
    const uint64_t hash = state->ZobristHash;
    auto it = Nodes.find(hash);
    if (it != Nodes.end()) {
      nextNode = it->second.get();
      X_ASSERT(nextNode->Children.size() == state->NumEmptyFields);
    } else {
      auto nextNodeIt = Nodes.emplace(hash, 
				      std::make_unique<Node>(hash, RandomPermutation(state->GetEmptyFields(), Random)));//.first->second.get();
      nextNode = nextNodeIt.first->second.get();
      X_ASSERT(state->NumEmptyFields == nextNode->Children.size());
    }
    node->Children[bestMove] = nextNode;
    ++nextNode->NumRefs;
  }
  const int8_t res = -PlayFrom(nextNode, state);
  node->Update(res);

  return res;
}

template<class GameState, class RandomGen>
void MCTS<GameState, RandomGen>::ApplyBestMove() {
  size_t bestMoveI = BestMove();
  X_ASSERT(bestMoveI < Root->Children.size());
  for (size_t i = 0; i < Root->Children.size(); ++i) {
    if (i == bestMoveI) {
      continue;
    }
    X_ASSERT(Root->Children[i] != nullptr);
    DeleteTree(Root->Children[i]);
  }
  const bool res = RootState->Place(Root->Actions[bestMoveI]);
  X_ASSERT(res);
  X_ASSERT(Root->Children[bestMoveI]);
  Node* newRoot = Root->Children[bestMoveI];
  Nodes.erase(Root->Hash);
  Root = newRoot;
}

template<class GameState, class RandomGen>
void MCTS<GameState, RandomGen>::DeleteTree(Node* node) {
  X_ASSERT(node != nullptr);
  X_ASSERT(node->NumRefs > 0);
  if (--node->NumRefs > 0) {
    return;
  }
  for (size_t i = 0; i < node->Children.size(); ++i) {
    Node* child = node->Children[i];
    if (child == nullptr) {
      continue;
    }
    DeleteTree(child);
  }
  Nodes.erase(node->Hash);
}

