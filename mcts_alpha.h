#pragma once

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <limits>
#include <numeric>
#include <unordered_map>
#include <vector>

#include "assert.h"

static const float EPS = 1e-8;

class MCTSNode {
public:
  std::vector<float> Qs;    // Q values for move #a (as defined in the paper)
  std::vector<uint32_t> Ns; // #times move #a was performed
  uint32_t N = 0;           // #times board was visited
  std::vector<float> Ps;    // policy values for moves
  //  float E = 0;              // stores game result
  std::vector<uint16_t> Vs; // stores valid moves
};

template<class Board>
class MCTS {
 public:
  std::vector<float> GetActionProbs(Board& canonicalBoard, float temperature = 1.f) {
    X_ASSERT(canonicalBoard.NumEmptyFields > 0);
    for (uint32_t i = 0; i < s_numMCTSSims; ++i) {
      Search(Board(canonicalBoard));
    }
    uint64_t hash = canonicalBoard.ZobristHash;
    MCTSNode& node = Nodes[hash];
    if (temperature == 0) {
      auto maxIt = std::max_element(node.Ns.begin(), node.Ns.end());
      std::vector<float> probs(node.Ns.size(), 0.f);
      probs[std::distance(node.Ns.begin(), maxIt)] = 1.f;
      return probs;
    }
    float sumCounts = 0.f;
    std::vector<float> counts;
    counts.reserve(node.Ns.size());
    for (uint32_t n : node.Ns) {
      const float count = std::pow(n, 1.f / temperature);
      counts.emplace_back(count);
      sumCounts += count;
    }
    X_ASSERT(sumCounts > 0.f);
    for (float& count : counts) {
      count /= sumCounts;
    }
    return counts;
  }

  float Search(Board& board) {
    if (board.NumEmptyFields == 0) {
      if (board.WhiteCaptured == board.BlackCaptured) {
	return -1e-3;
      }
      if (board.WhiteCaptured < board.BlackCaptured) {
	
      }
    }
    uint64_t hash = board.ZobristHash;
    MCTSNode& node = Nodes[hash];
    //    if (node.E != 0) {
      // terminal node
    //      return - node.E;
    //    }

    if (node.Ps.empty()) {
      // leaf node
      node.Ps.resize(board.NumEmptyFields, 0.f);
      float v=0;//node.P = nnet.predict(board)
      float sumP = std::accumulate(node.Ps.begin(), node.Ps.end(), 0);
      if (sumP > 0) {
	for (float& p : node.Ps) {
	  p /= sumP;
	}
      } else {
	fprintf(stderr, "All valid moves were masked, do workaround");
	std::fill(node.Ps.begin(), node.Ps.end(), 1.f / node.Ps.size());
	//	for (float& p : node.Ps) {
	//	  p = (p+1) / node.Ps.size();
	//	}
      }
      node.Vs = board.GetEmptyFields();
      node.Qs.resize(board.NumEmptyFields, 0.f);
      return -v;
    }
    float curBest = std::numeric_limits<float>::lowest();
    int bestAct = -1;
    X_ASSERT(board.NumEmptyFields == node.Qs.size());
    for (size_t i = 0; i < board.NumEmptyFields; ++i) {
      float u;
      if (node.Ns[i] > 0) {
	u = node.Qs[i] + s_cpuct*node.Ps[i]*std::sqrt(node.N)/(1+node.Ns[i]);
      } else {
	u = s_cpuct*node.Ps[i]*std::sqrt(node.N+EPS);  // Q = 0 ?
      }
      if (u > curBest) {
	curBest = u;
	bestAct = i;
      }
    }
    board.Place(node.Vs[bestAct]);

    const float v = Search(board);
    board.MakeCanonical();

    if (node.Ns[bestAct] > 0) {
      node.Qs[bestAct] = (node.Ns[bestAct]*node.Qs[bestAct]+v) / (node.Ns[bestAct] + 1);
      ++node.Ns[bestAct];
    } else {
      node.Qs[bestAct] = v;
      ++node.Ns[bestAct];
    }
    ++node.N;
    return -v;
  }

  // nnet
  std::unordered_map<uint64_t, MCTSNode> Nodes;
  static const uint32_t s_numMCTSSims;
  static const float s_cpuct;
};


template<class Board>
const uint32_t MCTS<Board>::s_numMCTSSims = 1000000;
template<class Board>
const float MCTS<Board>::s_cpuct = 1.f;
