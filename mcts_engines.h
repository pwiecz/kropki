#pragma once

#include <chrono>

#include "pcg_random.hpp"

#include "mcts_uct0.h"
#include "mcts_uct1.h"


class Engine {
 public:
  virtual void Place(uint16_t move) = 0;
  virtual void ThinkForIterations(uint32_t iterations) = 0;
  virtual uint32_t ThinkForTime(std::chrono::milliseconds time) = 0;
  virtual uint16_t BestMove() = 0;
};

template<class Board>
class RandomEngine : public Engine {
public:
  RandomEngine(Board board = Board())
    : m_board(std::move(board)) {
    //std::seed_seq seq{1, 2, 3, 4, 5};
    //    m_rand.seed(seq);
  }
  void Place(uint16_t move) override {
    m_board.Place(move);
  }
  void ThinkForIterations(uint32_t iterations) override {}
  uint32_t ThinkForTime(std::chrono::milliseconds time) override {return 0;}
  uint16_t BestMove() override {
    std::vector<uint16_t> moves = m_board.GetEmptyFields();
    return moves[m_rand() % moves.size()];
  }

private:
  pcg64 m_rand;
  Board m_board;
};


template<class Board, template<typename, typename> class MCTS>
class MCTSEngine : public Engine {
 public:
  MCTSEngine(Board board = Board());
  void Place(uint16_t move) override;
  void ThinkForIterations(uint32_t iterations) override;
  uint32_t ThinkForTime(std::chrono::milliseconds time) override;
  uint16_t BestMove() override;

 private:
  Board m_gameState;
  //  std::mt19937_64 m_rand;
  pcg64 m_rand;
  MCTS<Board, pcg64> m_mcts;
};

template<class Board>
using MCTS_UCT0_Engine = MCTSEngine<Board, MCTS_UCT0>;

template<class Board>
using MCTS_UCT1_Engine = MCTSEngine<Board, MCTS_UCT1>;

template<class Board, template<typename, typename> class MCTS>
MCTSEngine<Board, MCTS>::MCTSEngine(Board board)
  : m_gameState(std::move(board))
  , m_mcts(&m_gameState, &m_rand) {
  //  std::seed_seq seq{1, 2, 3, 4, 5};
  //  m_rand.seed(seq);
}

template<class Board, template<typename, typename> class MCTS>
void MCTSEngine<Board, MCTS>::Place(uint16_t move) {
  m_mcts.PlayMove(move);
}

template<class Board, template<typename, typename> class MCTS>
uint16_t MCTSEngine<Board, MCTS>::BestMove() {
  return m_mcts.BestMove();
}

template<class Board, template<typename, typename> class MCTS>
void MCTSEngine<Board, MCTS>::ThinkForIterations(uint32_t iterations) {
  while (iterations > 0) {
    m_mcts.Update();
    --iterations;
  }
}

template<class Board, template<typename, typename> class MCTS>
uint32_t MCTSEngine<Board, MCTS>::ThinkForTime(std::chrono::milliseconds time) {
  typedef std::chrono::steady_clock clock;
  clock::time_point start = clock::now();
  uint32_t iters = 0;
  while (clock::now() - start < time) {
    ThinkForIterations(100);
    iters += 100;
  }
  return iters;
}
