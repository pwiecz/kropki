#pragma once

#include "kropki.h"
#include "kropki_debug.h"

#include <algorithm>
#include <limits>
#include <memory>
#include <unordered_map>


/*class Node {
 public:
  Node(std::vector<uint16_t> actions);

  size_t PUCTIndex() const;
  void Select(size_t index);

  std::vector<uint16_t> Actions;
  std::vector<Node*> Children;

  std::vector<uint32_t> N;
  uint32_t m_sumN = 0;
  std::vector<float> Q;
  std::vector<float> P;

 private:
  static constexpt float m_cPUCT;
  static constexpr uint32_t m_nVL;
};

static float Node::m_cPUCT = 5.f;
static uint32_t Node::m_nVL = 5;

Node::Node(std::vector<uint16_t> actions)
  : Actions(std::move(actions))
  , Children(Actions.size(), nullptr)
  , N(Actions.size(), 0)
  , Q(Actions.size(), 0.f)
  , P(Actions.size(), 1.f / Actions.size()) {}

size_t Node::PUCTIndex() const {
  float bestV = std::numeric_limits<float>::lowest();
  size_t best;
  for (size_t i = 0; i < N.size(); ++i) {
    //if (N[i] == 0) {
    //  return i;
    //}
    float v = Q[i] + m_cPUCT * P[i] * std::sqrt(m_sumN) / (1 + N[i]);
    if (v > bestV) {
      bestV = i;
      best = i;
    }
  }
  if (bestV > std::numeric_limits<float>::lowest()) {
    return best;
  }
  assert(false);
  return 0;
}

void Node::Select(size_t index) {
  uint32_t n = N[index];
  uint32_t q = Q[index];

  N[index] += m_nVL;
  m_sumN += m_nVL;
  Q[index] = (n*q - m_nVL) / (n + m_nVL);
  }*/

class Node {
public:
  Node(std::vector<uint16_t> actions);

  void Update(size_t action, int8_t res);
  size_t BestIndex() const;

  const std::vector<uint16_t> Actions;
  std::vector<float> Wins;
  std::vector<uint32_t> Simulations;
  uint32_t TotalSimulations = 0;
  const float C;
};

Node::Node(std::vector<uint16_t> actions)
  : Actions(std::move(actions))
  , Wins(Actions.size(), 0.f)
  , Simulations(Actions.size(), 0)
  , C(std::sqrt(2.f)) {
  
}

void Node::Update(size_t action, int8_t res) {
  if (res > 0) {
    ++Wins[action];
  } else if (res == 0) {
    Wins[action] += 0.01f;
  }
  ++Simulations[action];
  ++TotalSimulations;
}

size_t Node::BestIndex() const {
  float bestScore = std::numeric_limits<float>::lowest();
  size_t bestIndex = 0;
  const float logN = TotalSimulations > 0 ? std::log((float)TotalSimulations) : 1;
  for (size_t i = 0; i < Actions.size(); ++i) {
    if (Simulations[i] == 0) {
      return i;
    }
    const float score = Wins[i] / Simulations[i] + C * std::sqrt(logN / Simulations[i]);
    if (score > bestScore) {
      bestScore = score;
      bestIndex = i;
    }
  }
  X_ASSERT(bestScore > std::numeric_limits<float>::lowest());
  return bestIndex;
}

template<class T, class RandomGen>
std::vector<T> RandomPermutation(std::vector<T>&& v, RandomGen* rand) {
  std::shuffle(v.begin(), v.end(), *rand);
  return v;
}

template<class GameState, class RandomGen>
class MCTS {
public:
  MCTS(GameState* state, RandomGen* rand);
  ~MCTS() {
    //    std::cerr << "Hash size: " << Nodes.size();
  }
  void Update();

  uint16_t BestMove() const;
  void ApplyBestMove();

private:
  int8_t PlayFrom(Node* node, GameState* state);
  GameState* const RootState;
  RandomGen* const Random;
  Node* Root;
  std::unordered_map<uint64_t, std::unique_ptr<Node>> Nodes;
};

template<class GameState, class RandomGen>
MCTS<GameState, RandomGen>::MCTS(GameState* state, RandomGen* rand)
  : RootState(state)
  , Random(rand)
  , Root(new Node(RandomPermutation(RootState->GetEmptyFields(), rand))) {}

template<class GameState, class RandomGen>
void MCTS<GameState, RandomGen>::Update() {
  GameState currentState = *RootState;
  PlayFrom(Root, &currentState);
}

template<class GameState, class RandomGen>
uint16_t MCTS<GameState, RandomGen>::BestMove() const {
  uint32_t bestNumVisits = 0;
  uint16_t bestMove = 0;
  //  size_t bestMoveI;
  for (size_t i = 0; i < Root->Actions.size(); ++i) {
    if (Root->Simulations[i] > bestNumVisits) {
      bestNumVisits = Root->Simulations[i];
      bestMove = Root->Actions[i];
      //      bestMoveI = i;
    }
  }
  X_ASSERT(bestNumVisits > 0);
  //  std::cerr << "Best move(" << bestMoveI <<") visits: " << bestNumVisits << " wins: " <<Root->Wins[bestMoveI] << "\n";
  return bestMove;
}

template<class GameState, class RandomGen>
int8_t MCTS<GameState, RandomGen>::PlayFrom(Node* node, GameState* state) {
  if (state->NumEmptyFields == 0) {
    //    std::cerr << "Side: " << SideToString(state->SideToMove) << " WhiteCap: " << state->WhiteCaptured << " BlackCap: " << state->BlackCaptured << " Final result: " << (int)state->Result() << "\n";
    return state->Result();
  }

  size_t bestMove = node->BestIndex();
  state->Place(node->Actions[bestMove]);
  //  std::cerr << "best rollout wins: " << node->Wins[bestMove] << "\n";
  const uint64_t hash = state->ZobristHash;
  Node* nextNode;
  auto it = Nodes.find(hash);
  if (it != Nodes.end()) {
    nextNode = it->second.get();
  } else {
    //    std::vector<uint16_t> emptyFields = state->GetEmptyFields();
    //    std::shuffle(emptyFields.begin(), emptyFields.end(), *Random);
    nextNode = Nodes.emplace(hash, 
			     std::make_unique<Node>(RandomPermutation(state->GetEmptyFields(), Random))).first->second.get();
  }
  int8_t res = -PlayFrom(nextNode, state);
  node->Update(bestMove, res);

  return res;
}

template<class GameState, class RandomGen>
void MCTS<GameState, RandomGen>::ApplyBestMove() {
  RootState->Place(BestMove());
  const uint64_t hash = RootState->ZobristHash;
  auto it = Nodes.find(hash);
  if (it != Nodes.end()) {
    Root = it->second.get();
    return;
  }
  std::unique_ptr<Node> newNode = std::make_unique<Node>(RootState->GetEmptyFields());
  Root = Nodes.emplace(hash, std::move(newNode)).first->second.get();
}
