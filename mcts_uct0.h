#pragma once

#include <cmath>
#include <cstdio>
#include <cstdint>
#include <limits>
#include <memory>
#include <random>

#include "assert.h"
//#include "kropki.h"
//#include "kropki_debug.h"


template<class GameState, class Rand, bool FreeList=true>
class MCTS_UCT0 {
private:
  class Node;
public:
  MCTS_UCT0(GameState* state, Rand* rand);
  ~MCTS_UCT0();
  void Update();

  void PlayMove(uint16_t move);
  uint16_t BestMove() const;
  uint16_t ApplyBestMove();

  void DeleteNode(Node* node);
  Node* CreateNode(Node* parent, size_t numChildren);

private:
  int8_t PlayFrom(Node* node, GameState* state);
  int8_t RandomPlayoutFrom(Node* node, GameState* state);
  size_t BestMoveIndex() const;

  GameState* const m_rootState;
  Rand* const m_random;
  Node* m_root;
  Node* m_freeList = nullptr;
  size_t m_freeListSize = 0;

  class Node {
  public:
    Node(Node* parent, std::vector<uint16_t> actions);
    Node(Node* parent, size_t numChildren);

    void Update(int8_t res);
    size_t BestIndex() const;

    Node* Parent;
    std::vector<uint16_t> Actions;
    std::vector<Node*> Children;
    float Wins = 0.f;
    uint32_t Simulations = 0;
    uint32_t TotalSimulations = 0;
    uint32_t NumRefs = 0;

    static const float C;
  };
};

template<class GameState, class Rand, bool FreeList>
MCTS_UCT0<GameState, Rand, FreeList>::MCTS_UCT0(GameState* state, Rand* rand)
  : m_rootState(state)
  , m_random(rand)
  , m_root(new Node(nullptr, m_rootState->GetEmptyFields())) {
}

template<class GameState, class Rand, bool FreeList>
MCTS_UCT0<GameState, Rand, FreeList>::~MCTS_UCT0() {
  if constexpr (FreeList) {
      //    fprintf(stderr, "FreeListSize ~ %d\n", (int)m_freeListSize);
    while (m_freeList != nullptr) {
      Node* node = m_freeList;
      m_freeList = m_freeList->Parent;
      delete node;
    }
  }
  DeleteNode(m_root);
}

template<class GameState, class Rand, bool FreeList>
void MCTS_UCT0<GameState, Rand, FreeList>::Update() {
  GameState currentState = *m_rootState;
  PlayFrom(m_root, &currentState);
}

template<class GameState, class Rand, bool FreeList>
uint16_t MCTS_UCT0<GameState, Rand, FreeList>::BestMove() const {
  size_t bestMoveI = BestMoveIndex();
  X_ASSERT(bestMoveI < m_root->Actions.size());
  Node* child = m_root->Children[bestMoveI];
  fprintf(stderr, "Wins: %f/%d\n", child->Wins, (int)child->Simulations);
  if (child->Wins <= 0) {
    // resign
    return 0;
  }
  return m_root->Actions[bestMoveI];
}

template<class GameState, class Rand, bool FreeList>
size_t MCTS_UCT0<GameState, Rand, FreeList>::BestMoveIndex() const {
  uint32_t bestNumVisits = 0;
  size_t bestMoveI = 0;
  for (size_t i = 0; i < m_root->Children.size(); ++i) {
    Node const* child = m_root->Children[i];
    if (!child) {
      X_ASSERT(false);
      continue;
    }
    if (child->Simulations > bestNumVisits) {
      bestNumVisits = child->Simulations;
      bestMoveI = i;
    }
  }
  X_ASSERT(bestNumVisits > 0);
  return bestMoveI;
}

template<class GameState, class Rand, bool FreeList>
int8_t MCTS_UCT0<GameState, Rand, FreeList>::PlayFrom(Node* node, GameState* state) {
  if (state->NumEmptyFields == 0) {
    const int8_t res = state->Result();
    node->Update(-res);
    return res;
  }

  const size_t bestMove = node->BestIndex();
  X_ASSERT(state->CanBePlayed(node->Actions[bestMove]));
  state->Place(node->Actions[bestMove]);
  Node** child = &node->Children[bestMove];
  if (!*child) {
    *child = CreateNode(node, state->NumEmptyFields);
    state->GetEmptyFields(&(*child)->Actions);
    const int8_t res = RandomPlayoutFrom(*child, state);
    node->Update(res);
    return -res;
  }
  const int8_t res = PlayFrom(*child, state);
  node->Update(res);

  return -res;
}

template<class GameState, class Rand, bool FreeList>
int8_t MCTS_UCT0<GameState, Rand, FreeList>::RandomPlayoutFrom(Node* node, GameState* state) {
  int8_t side = -1;
  while (state->NumEmptyFields > 0) {
    std::uniform_int_distribution<int16_t> dst(0, state->NumEmptyFields - 1);
    const uint16_t action = state->GetNthEmptyField(dst(*m_random));
    X_ASSERT(state->CanBePlayed(action));
    //    fprintf(stderr, "%s\n", state->BoardToString());
    state->Place(action);
    side = -side;
  }
  int8_t res = state->Result();
  node->Update(side * res);
  return -side * res;
}

template<class GameState, class Rand, bool FreeList>
uint16_t MCTS_UCT0<GameState, Rand, FreeList>::ApplyBestMove() {
  size_t bestMoveI = BestMoveIndex();
  X_ASSERT(bestMoveI < m_root->Children.size());
  const uint16_t bestMove = m_root->Actions[bestMoveI];
  X_ASSERT(m_rootState->CanBePlayed(bestMove));
  m_rootState->Place(bestMove);
  Node* newRoot = m_root->Children[bestMoveI];
  X_ASSERT(newRoot);
  m_root->Children[bestMoveI] = nullptr;
  DeleteNode(m_root);
  m_root = newRoot;
  m_root->Parent = nullptr;
  return bestMove;
}

template<class GameState, class Rand, bool FreeList>
void MCTS_UCT0<GameState, Rand, FreeList>::PlayMove(uint16_t move) {
  size_t bestMoveI = 0;
  bool foundMove = false;
  for (size_t i = 0; i < m_root->Actions.size(); ++i) {
    if (m_root->Actions[i] == move) {
      bestMoveI = i;
      foundMove = true;
      break;
    }
  }
  X_ASSERT(foundMove);
  X_ASSERT(m_rootState->CanBePlayed(move));
  m_rootState->Place(move);
  Node* newRoot = m_root->Children[bestMoveI];
  m_root->Children[bestMoveI] = nullptr;
  if (!newRoot) {
    newRoot = CreateNode(nullptr, m_rootState->NumEmptyFields);
    m_rootState->GetEmptyFields(&newRoot->Actions);
  }
  DeleteNode(m_root);
  m_root = newRoot;
  m_root->Parent = nullptr;
}

template<class GameState, class Rand, bool FreeList>
void MCTS_UCT0<GameState, Rand, FreeList>::DeleteNode(Node* node) {
  for (Node* child : node->Children) {
    if (child != nullptr) {
      DeleteNode(child);
    }
  }
  if constexpr(!FreeList) {
    delete node;
    return;
  }
  if (m_freeListSize < 1000000) {
    node->Parent = m_freeList;
    m_freeList = node;
    ++m_freeListSize;
  } else {
    delete node;
  }
}

template<class GameState, class Rand, bool FreeList>
typename MCTS_UCT0<GameState, Rand, FreeList>::Node* 
MCTS_UCT0<GameState, Rand, FreeList>::CreateNode(Node* parent, size_t numChildren) {
  if constexpr(!FreeList) {
    return new Node(parent, numChildren);
  }
  if (m_freeList == nullptr) {
    return new Node(parent, numChildren);
  }
  X_ASSERT(m_freeListSize > 0);
  Node* node = m_freeList;
  m_freeList = m_freeList->Parent;
  --m_freeListSize;
  node->Parent = parent;
  node->Actions.resize(numChildren);
  node->Children.resize(numChildren);
  std::fill(node->Children.begin(), node->Children.end(), nullptr);
  node->Wins = 0;
  node->Simulations = 0;
  node->TotalSimulations = 0;
  node->NumRefs = 0;
  return node;
}

template<class GameState, class Rand, bool FreeList>
const float MCTS_UCT0<GameState, Rand, FreeList>::Node::C = std::sqrt(2.f);

template<class GameState, class Rand, bool FreeList>
MCTS_UCT0<GameState, Rand, FreeList>::Node::Node(Node* parent, std::vector<uint16_t> actions)
  : Parent(parent) 
  , Actions(std::move(actions))
  , Children(Actions.size(), nullptr) {}

template<class GameState, class Rand, bool FreeList>
MCTS_UCT0<GameState, Rand, FreeList>::Node::Node(Node* parent, size_t numChildren)
  : Parent(parent) 
  , Actions(numChildren)
  , Children(numChildren, nullptr) {}

template<class GameState, class Rand, bool FreeList>
void MCTS_UCT0<GameState, Rand, FreeList>::Node::Update(int8_t res) {
  if (res > 0) {
    Wins = Wins + 1 + (float)res/(1000.f*(GameState::Width*GameState::Height));
  } else if (res == 0) {
    Wins += 0.01f;
  } else if (res < 0) {
    Wins = Wins + (float)res/(1000.f*(GameState::Width*GameState::Height));
  }
  ++Simulations;
  if (Parent) {
    ++(Parent->TotalSimulations);
  }
}

template<class GameState, class Rand, bool FreeList>
size_t MCTS_UCT0<GameState, Rand, FreeList>::Node::BestIndex() const {
  float bestScore = std::numeric_limits<float>::lowest();
  size_t bestIndex = 0;

  const float logN = (TotalSimulations > 0) ? std::log((float)TotalSimulations) : 1.f;

  for (size_t i = 0; i < Children.size(); ++i) {
    Node const* child = Children[i];
    if (!child) {
      return i;
    }
    const float score = child->Wins / child->Simulations + C * std::sqrt(logN / child->Simulations);
    if (score > bestScore) {
      bestScore = score;
      bestIndex = i;
    }
  }
  X_ASSERT(bestScore > std::numeric_limits<float>::lowest());
  return bestIndex;
}
