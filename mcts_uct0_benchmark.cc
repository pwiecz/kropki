#include "pcg_random.hpp"

#include "kropki.h"
#include "kropki_parent.h"
#include "mcts_uct0.h"


#define USE_KROPKI_PARENT
#ifdef USE_KROPKI_PARENT
using kropki_parent::Board;
#else
using kropki::Board;
#endif

int main() {
  constexpr uint16_t W = 10, H = 10;
  pcg64 rand;
#ifndef USE_KROPKI_PARENT
  Board<W, H>::InitializeHashes(rand);
#endif
  for (int i = 0; i < 100; ++i) {
  Board<W, H> board;
  MCTS_UCT0<Board<W,H>, pcg64> mcts(&board, &rand);
  while (board.NumEmptyFields > 0) {
      for (int i = 0; i < 1000; ++i) {
	mcts.Update();
      }
      //board.Place(mcts.BestMove());
      mcts.ApplyBestMove();
    //    fprintf(stderr, "%d\n", (int)board.NumEmptyFields);
  }
  }
  return 0;
}


