#pragma once

#include "kropki.h"
#include "kropki_debug.h"

#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <random>
#include <unordered_map>


template<class GameState, class Rand>
class MCTS_UCT1_2 {
 private:
  class Node;
 public:
  MCTS_UCT1_2(GameState* state, Rand* rand);

  void Update();

  uint16_t BestMove() const;
  void PlayMove(uint16_t move);
  void ApplyBestMove();

 private:
  void DeleteTree(Node* node);
  int8_t PlayFrom(Node* node, GameState* state);
  int8_t RandomPlayoutFrom(Node* node, GameState* state);

  GameState* const RootState;
  Rand* const Random;
  Node* Root;

  class Node {
  public:
    Node(std::vector<uint16_t> actions);

    void Update(size_t action, int8_t res);
    size_t BestIndex() const;

    const std::vector<uint16_t> Actions;
    std::vector<Node*> Children;
    std::vector<float> Wins;
    std::vector<uint32_t> Simulations;
    uint32_t TotalSimulations = 0;
    static const float C;
  };

};

template<class GameState, class Rand>
MCTS_UCT1_2<GameState, Rand>::MCTS_UCT1_2(GameState* state, Rand* rand)
  : RootState(state)
  , Random(rand)
  , Root(new Node(RootState->GetEmptyFields())) {
}

template<class GameState, class Rand>
void MCTS_UCT1_2<GameState, Rand>::Update() {
  GameState currentState = *RootState;
  PlayFrom(Root, &currentState);
}

template<class GameState, class Rand>
uint16_t MCTS_UCT1_2<GameState, Rand>::BestMove() const {
  uint32_t bestNumVisits = 0;
  uint16_t bestMove = 0;
  for (size_t i = 0; i < Root->Actions.size(); ++i) {
    if (Root->Simulations[i] > bestNumVisits) {
      bestNumVisits = Root->Simulations[i];
      bestMove = Root->Actions[i];
    }
  }
  X_ASSERT(bestNumVisits > 0);
  return bestMove;
}

template<class GameState, class Rand>
int8_t MCTS_UCT1_2<GameState, Rand>::PlayFrom(Node* node, GameState* state) {
  if (state->NumEmptyFields == 0) {
    return state->Result();
  }

  size_t bestMoveI = node->BestIndex();
  state->Place(node->Actions[bestMoveI]);
  const uint64_t hash = state->ZobristHash;
  Node* nextNode = node->Children[bestMoveI];
  if (nextNode == nullptr) {
    nextNode = new Node(state->GetEmptyFields());
    node->Children[bestMoveI] = nextNode;
    const int8_t res = -RandomPlayoutFrom(nextNode, state);
    node->Update(bestMoveI, res);
    return res;
  }
  int8_t res = -PlayFrom(nextNode, state);
  node->Update(bestMoveI, res);

  return res;
}

template<class GameState, class Rand>
int8_t MCTS_UCT1_2<GameState, Rand>::RandomPlayoutFrom(Node* node, GameState* state) {
  X_ASSERT(!node->Children.empty());
  int8_t side = 1;
  while (state->NumEmptyFields > 0) {
    std::uniform_int_distribution<int16_t> dst(0, state->NumEmptyFields - 1);
    const uint16_t action = state->GetNthEmptyField(dst(*Random));
    state->Place(action);
    side = -side;
  }
  int8_t res = state->Result();
  return side * res;
}

template<class GameState, class Rand>
void MCTS_UCT1_2<GameState, Rand>::ApplyBestMove() {
  PlayMove(BestMove());
}

template<class GameState, class Rand>
void MCTS_UCT1_2<GameState, Rand>::PlayMove(uint16_t move) {
  size_t bestMoveI;
  bool foundMove = false;
  for (size_t i = 0; i < Root->Actions.size(); ++i) {
    if (Root->Actions[i] == move) {
      bestMoveI = i;
      foundMove = true;
    } else if (Root->Children[i] != nullptr) {
      DeleteTree(Root->Children[i]);
      Root->Children[i] = nullptr;
    }
  }
  X_ASSERT(foundMove);
  bool res = RootState->Place(move);
  X_ASSERT(res);
  Node* newRoot = Root->Children[bestMoveI];
  Root->Children[bestMoveI] = nullptr;
  if (newRoot == nullptr) {
    newRoot = new Node(RootState->GetEmptyFields());
    //    Root->Children[bestMoveI] = newRoot;
  }
  Root->Children[bestMoveI] = nullptr;
  DeleteTree(Root);
  Root = newRoot;
}

template<class GameState, class Rand>
void MCTS_UCT1_2<GameState, Rand>::DeleteTree(Node* node) {
  for (size_t i = 0; i < node->Children.size(); ++i) {
    Node* child = node->Children[i];
    if (child == nullptr) {
      continue;
    }
    DeleteTree(child);
  }
  delete node;
}

template<class GameState, class Rand>
MCTS_UCT1_2<GameState, Rand>::Node::Node(std::vector<uint16_t> actions)
  : Actions(std::move(actions))
  , Children(Actions.size(), nullptr)
  , Wins(Actions.size(), 0.f)
  , Simulations(Actions.size(), 0) {}

template<class GameState, class Rand>
void MCTS_UCT1_2<GameState, Rand>::Node::Update(size_t action, int8_t res) {
  if (res > 0) {
    ++Wins[action];
  } else if (res == 0) {
    Wins[action] += 0.01f;
  }
  ++Simulations[action];
  ++TotalSimulations;
}

template<class GameState, class Rand>
size_t MCTS_UCT1_2<GameState, Rand>::Node::BestIndex() const {
  float bestScore = std::numeric_limits<float>::lowest();
  size_t bestIndex = 0;
  const float logN = TotalSimulations > 0 ? std::log((float)TotalSimulations) : 1;
  for (size_t i = 0; i < Actions.size(); ++i) {
    if (Simulations[i] == 0) {
      return i;
    }
    const float score = Wins[i] / Simulations[i] + C * std::sqrt(logN / Simulations[i]);
    if (score > bestScore) {
      bestScore = score;
      bestIndex = i;
    }
  }
  X_ASSERT(bestScore > std::numeric_limits<float>::lowest());
  return bestIndex;
}

template<class GameState, class Rand>
const float MCTS_UCT1_2<GameState, Rand>::Node::C = std::sqrt(2.f);
