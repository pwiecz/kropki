#include "pcg_random.hpp"
#include <random>

#include "kropki.h"
#include "mcts_uct1.h"


using kropki::Board;

int main() {
  constexpr uint16_t W = 10, H = 10;
  pcg64 rand;
  Board<W, H>::InitializeHashes(rand);
  Board<W, H> board;
  MCTS_UCT1<Board<W,H>, pcg64> mcts(&board, &rand);
  while (board.NumEmptyFields > 0) {
    for (int i = 0; i < 1000; ++i) {
      mcts.Update();
    }
    //board.Place(mcts.BestMove());
    mcts.ApplyBestMove();
    //    std::cerr << board.NumEmptyFields << "\n";
  }
  return 0;
}


