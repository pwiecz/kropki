#pragma once

#include <cstdint>

#include "xassert.h"

enum class Side {
  WHITE, BLACK,
};
constexpr inline Side OppositeSide(Side side) {
  switch(side) {
  case Side::WHITE: return Side::BLACK;
  case Side::BLACK: return Side::WHITE;
  }
  X_ASSERT(false);
  return Side::WHITE;
}
enum class FieldState : uint8_t {
  EMPTY,
    WHITE, BLACK,
    CAPTURED_WHITE, CAPTURED_BLACK,
    EMPTY_CAPTURED_BY_WHITE, EMPTY_CAPTURED_BY_BLACK,
    EMPTY_SURROUNDED_BY_WHITE, EMPTY_SURROUNDED_BY_BLACK,
    NUM_STATES,
};

enum class FillMode {
  SURROUND, CAPTURE, EMPTY,
};

constexpr inline FieldState OppositeState(FieldState state) {
  switch(state) {
  case FieldState::EMPTY: return FieldState::EMPTY;
  case FieldState::WHITE: return FieldState::BLACK;
  case FieldState::BLACK: return FieldState::WHITE;
  case FieldState::CAPTURED_WHITE: return FieldState::CAPTURED_BLACK;
  case FieldState::CAPTURED_BLACK: return FieldState::CAPTURED_WHITE;
  case FieldState::EMPTY_CAPTURED_BY_WHITE: return FieldState::EMPTY_CAPTURED_BY_BLACK;
  case FieldState::EMPTY_CAPTURED_BY_BLACK: return FieldState::EMPTY_CAPTURED_BY_WHITE;
  case FieldState::EMPTY_SURROUNDED_BY_WHITE: return FieldState::EMPTY_SURROUNDED_BY_BLACK;
  case FieldState::EMPTY_SURROUNDED_BY_BLACK: return FieldState::EMPTY_SURROUNDED_BY_WHITE;
  case FieldState::NUM_STATES: X_ASSERT(false); return FieldState::EMPTY;
  }
  X_ASSERT(false);
  return FieldState::EMPTY;
}

constexpr inline FieldState SideToFieldState(Side side) {
  switch (side) {
  case Side::WHITE: return FieldState::WHITE;
  case Side::BLACK: return FieldState::BLACK;
  }
  X_ASSERT(false);
  return FieldState::BLACK;
}

template<Side side>
bool PieceIsOfSide(FieldState piece) {
  if constexpr(side == Side::WHITE) {
    return piece == FieldState::WHITE;
  } else {
    return piece == FieldState::BLACK;
  }
}
