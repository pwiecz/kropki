from setuptools import Extension, setup
from Cython.Build import cythonize

setup(
    name="pykropki",
    version="0.0.3",
    ext_modules=cythonize(
        Extension(
            "pykropki", ["kropki_ext.pyx"],
            extra_compile_args=["-std=c++17"]
        )
    )
)
