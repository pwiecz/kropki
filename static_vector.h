#pragma once

#include <array>
#include <cstddef>
#include <type_traits>

#include "xassert.h"


template<typename T, size_t N>
class static_vector {
  static_assert(std::is_trivially_destructible_v<T>);
  static_assert(std::is_trivially_default_constructible_v<T>);
 public:
  typedef T* iterator;
  typedef T const * const_iterator;

  bool empty() const noexcept;
  size_t size() const noexcept;

  void clear() noexcept;
  void resize(size_t n) noexcept;
  void push_back(const T& v) noexcept(std::is_nothrow_copy_constructible_v<T>);
  void push_back(T&& v) noexcept(std::is_nothrow_move_constructible_v<T>);
  template<class... Args>
  void emplace_back(Args&&... args) noexcept(std::is_nothrow_constructible_v<T, Args...>);
  void pop_back() noexcept;

  iterator begin() noexcept;
  const_iterator begin() const noexcept;
  const_iterator cbegin() const noexcept;
  iterator end() noexcept;
  const_iterator end() const noexcept;
  const_iterator cend() const noexcept;

  T& back() noexcept;
  const T& back() const noexcept;

  T& front() noexcept;
  const T& front() const noexcept;

  T& operator[](size_t n) noexcept;
  const T& operator[](size_t n) const noexcept;

 private:
  typename std::aligned_storage<sizeof(T), alignof(T)>::type m_data[N];
  size_t m_size = 0;
};

template<typename T, size_t N>
bool static_vector<T, N>::empty() const noexcept {
  return m_size == 0;
}
template<typename T, size_t N>
size_t static_vector<T, N>::size() const noexcept {
  return m_size;
}

template<typename T, size_t N>
void static_vector<T, N>::clear() noexcept {
  m_size = 0;
}
template<typename T, size_t N>
void static_vector<T, N>::resize(size_t n) noexcept {
  m_size = n;
}

template<typename T, size_t N>
void static_vector<T, N>::push_back(const T& v) noexcept(std::is_nothrow_copy_constructible_v<T>) {
  X_ASSERT(m_size < N);
  new(&m_data[m_size]) T(v);
  ++m_size;
}
template<typename T, size_t N>
void static_vector<T, N>::push_back(T&& v) noexcept(std::is_nothrow_move_constructible_v<T>) {
  X_ASSERT(m_size < N);
  new(&m_data[m_size]) T(std::move(v));
  ++m_size;
}
template<typename T, size_t N>
template<class... Args>
void static_vector<T, N>::emplace_back(Args&&... args) noexcept(std::is_nothrow_constructible_v<T, Args...>) {
  X_ASSERT(m_size < N);
  new(&m_data[m_size]) T(std::forward<Args>(args)...);
  ++m_size;
}

template<typename T, size_t N>
void static_vector<T, N>::pop_back() noexcept {
  X_ASSERT(m_size > 0);
  --m_size;
}

template<typename T, size_t N>
typename static_vector<T, N>::iterator static_vector<T, N>::begin() noexcept {
  return std::launder(reinterpret_cast<T*>(&m_data[0]));
}
template<typename T, size_t N>
typename static_vector<T, N>::const_iterator static_vector<T, N>::begin() const noexcept {
  return std::launder(reinterpret_cast<const T*>(&m_data[0]));
}
template<typename T, size_t N>
typename static_vector<T, N>::const_iterator static_vector<T, N>::cbegin() const noexcept {
  return std::launder(reinterpret_cast<const T*>(&m_data[0]));
}
template<typename T, size_t N>
typename static_vector<T, N>::iterator static_vector<T, N>::end() noexcept {
  return std::launder(reinterpret_cast<T*>(&m_data[m_size]));
}
template<typename T, size_t N>
typename static_vector<T, N>::const_iterator static_vector<T, N>::end() const noexcept {
  return std::launder(reinterpret_cast<const T*>(&m_data[m_size]));
}
template<typename T, size_t N>
typename static_vector<T, N>::const_iterator static_vector<T, N>::cend() const noexcept {
  return std::launder(reinterpret_cast<const T*>(&m_data[m_size]));
}

template<typename T, size_t N>
T& static_vector<T, N>::back() noexcept {
  X_ASSERT(m_size > 0);
  return *std::launder(reinterpret_cast<T*>(&m_data[m_size - 1]));
}
template<typename T, size_t N>
const T& static_vector<T, N>::back() const noexcept {
  X_ASSERT(m_size > 0);
  return *std::launder(reinterpret_cast<const T*>(&m_data[m_size - 1]));
}
template<typename T, size_t N>
T& static_vector<T, N>::front() noexcept {
  X_ASSERT(m_size > 0);
  return *std::launder(reinterpret_cast<T*>(&m_data[0]));
}
template<typename T, size_t N>
const T& static_vector<T, N>::front() const noexcept {
  X_ASSERT(m_size > 0);
  return *std::launder(reinterpret_cast<const T*>(&m_data[m_size - 1]));
}
template<typename T, size_t N>
T& static_vector<T, N>::operator[](size_t n) noexcept {
  X_ASSERT(n < m_size);
  return *std::launder(reinterpret_cast<T*>(&m_data[n]));
}
template<typename T, size_t N>
const T& static_vector<T, N>::operator[](size_t n) const noexcept {
  X_ASSERT(n < m_size);
  return *std::launder(reinterpret_cast<const T*>(&m_data[n]));
}
