#pragma once

#include <functional>
#include <vector>

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>

#include "../assert.h"

#include "../pieces.h"

template<class Board>
class Fl_Board : public Fl_Widget {
public:
  Fl_Board(int x, int y, int width, int height, const char* label = nullptr)
    : Fl_Widget(x, y, width, height, label)
    , m_x(x), m_y(y), m_width(width), m_height(height), m_lastX(-1), m_lastY(-1) {}

  Board* board() {
    return &m_board;
  }

  void addOnMoveCallback(std::function<bool(int x, int y)> moveCallback) {
    m_moveCallbacks.emplace_back(moveCallback);
  }

  void acceptMoves(bool accept) {
    m_acceptMoves = accept;
  }

  void setLastMove(int x, int y) {
    m_lastX = x;
    m_lastY = y;
  }

  void draw() override {
    fl_draw_box(FL_FLAT_BOX, m_x, m_y, m_width, m_height, FL_DARK_YELLOW);
    fl_color(FL_BLACK);
    const float fieldWidth = (float)m_width / (float)Board::Width;
    for (int x = 0; x <= Board::Width; ++x) {
      fl_line(m_x + (int)(x * fieldWidth), m_y, m_x + (int)(x * fieldWidth), m_y + m_height);
    }
    const float fieldHeight = (float)m_height / (float)Board::Height;
    for (int y = 0; y <= Board::Height; ++y) {
      fl_line(m_x, m_y + (int)(y * fieldHeight), m_x + m_width, m_y + (int)(y * fieldHeight));
    }
    
    for (int16_t x = 0; x < Board::Width; ++x) {
      for (int16_t y = 0; y < Board::Height; ++y) {
	float centerX = m_x + fieldWidth * (x + 0.5f), centerY = m_y + fieldHeight * (y + 0.5f);
	FieldState piece = m_board.PieceAt(x, y);
	switch (piece) {
	case FieldState::EMPTY: break;
	case FieldState::WHITE:
	  fl_color(FL_WHITE);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 3);
	  break;
	case FieldState::BLACK:
	  fl_color(FL_BLACK);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 3);
	  break;
	case FieldState::CAPTURED_WHITE:
	  fl_color(FL_WHITE);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 5);
	  break;
	case FieldState::CAPTURED_BLACK:
	  fl_color(FL_BLACK);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 5);
	  break;
	case FieldState::EMPTY_SURROUNDED_BY_WHITE:
	  fl_color(FL_WHITE);
	  fl_point(centerX, centerY);
	  break;
	case FieldState::EMPTY_SURROUNDED_BY_BLACK:
	  fl_color(FL_BLACK);
	  fl_point(centerX, centerY);
	  break;
	case FieldState::EMPTY_CAPTURED_BY_WHITE:
	  fl_color(FL_WHITE);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 10);
	  break;
	case FieldState::EMPTY_CAPTURED_BY_BLACK:
	  fl_color(FL_BLACK);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 10);
	  break;
	default:
	  X_ASSERT(false);
	}
	if (x == m_lastX && y == m_lastY) {
	  if (piece == FieldState::WHITE) {
	    fl_color(FL_BLACK);
	    DrawFilledCircle(centerX, centerY, fieldWidth / 7);
	  } else {
	    fl_color(FL_WHITE);
	    DrawFilledCircle(centerX, centerY, fieldWidth / 7);
	  }
	}
      }
    }
  }

  void DrawArrow(float cx, float cy, float length, float dx, float dy) {
    fl_line(cx + (-length * dx/ 2), cy + (-length * dy/2), cx + length * dx/2, cy+length *dy/2);
    DrawFilledCircle(cx+length*dx/2, cy+length*dy/2, length / 4);
  }

  void DrawFilledCircle(float cx, float cy, float radius) {
    fl_pie(cx - radius, cy - radius, 2*radius, 2*radius, 0, 360);
  }

  int handle(int event) override {
    if (event == FL_PUSH) {
      if (!m_acceptMoves) {
	return 0;
      }
      const int x = (Fl::event_x() - m_x) * Board::Width / m_width;
      const int y = (Fl::event_y() - m_y) * Board::Height / m_height;
      //      std::cout << "xy " << x << "," << y << "\n";
      //      MakeMove(x, y);
      for (auto& c : m_moveCallbacks) {
	c(x, y);
      }
      redraw();
      Fl::flush();
      return 1;
    }
    return 0;
  }

  //  void MakeMove(int x, int y) {
  //    m_board.Place(x, y);
  //    redraw();
  //  }

private:
  int m_x, m_y;
  int m_width, m_height;
  bool m_acceptMoves = true;
  Board m_board;
  int m_lastX, m_lastY;
  std::vector<std::function<bool(int x, int y)>> m_moveCallbacks;
};
