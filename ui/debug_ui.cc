#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>

#include "kropki.h"


using kropki::Board;
using kropki::FieldState;
using kropki::Side;

template<uint16_t W, uint16_t H>
class Fl_Board : public Fl_Widget {
public:
  Fl_Board(int x, int y, int width, int height, kropki::Board<W, H>* board)
    : Fl_Widget(x, y, width, height)
    , m_x(x), m_y(y), m_width(width), m_height(height), m_board(board) {}
  
  void draw() override {
    fl_draw_box(FL_FLAT_BOX, m_x, m_y, m_width, m_height, FL_DARK_YELLOW);
    fl_color(FL_BLACK);
    const float fieldWidth = (float)m_width / (float)W;
    for (int x = 0; x <= W; ++x) {
      fl_line(m_x + (int)(x * fieldWidth), m_y, m_x + (int)(x * fieldWidth), m_y + m_height);
    }
    const float fieldHeight = (float)m_height / (float)H;
    for (int y = 0; y <= H; ++y) {
      fl_line(m_x, m_y + (int)(y * fieldHeight), m_x + m_width, m_y + (int)(y * fieldHeight));
    }
    
    for (int16_t x = 0; x < W; ++x) {
      for (int16_t y = 0; y < H; ++y) {
	float centerX = m_x + fieldWidth * (x + 0.5f), centerY = m_y + fieldHeight * (y + 0.5f);
	FieldState piece = m_board->PieceAt(x, y);
	Side pieceSide;
	switch (piece) {
	case FieldState::EMPTY: break;
	case FieldState::WHITE: 
	  pieceSide = Side::WHITE;
	  fl_color(FL_WHITE);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 3);
	  break;
	case FieldState::BLACK:
	  pieceSide = Side::BLACK;
	  fl_color(FL_BLACK);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 3);
	  break;
	case FieldState::WHITE_WALL:
	  pieceSide = Side::WHITE;
	  fl_color(FL_WHITE);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 2.1);
	  break;
	case FieldState::BLACK_WALL:
	  pieceSide = Side::BLACK;
	  fl_color(FL_BLACK);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 2.1);
	  break;
	case FieldState::CAPTURED_WHITE:
	  pieceSide = Side::WHITE;
	  fl_color(FL_WHITE);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 5);
	  break;
	case FieldState::CAPTURED_BLACK:
	  pieceSide = Side::BLACK;
	  fl_color(FL_BLACK);
	  DrawFilledCircle(centerX, centerY, fieldWidth / 5);
	  break;
	case FieldState::EMPTY_SURROUNDED_BY_WHITE:
	  fl_color(FL_WHITE);
	  fl_point(centerX, centerY);
	  break;
	case FieldState::EMPTY_SURROUNDED_BY_BLACK:
	  fl_color(FL_BLACK);
	  fl_point(centerX, centerY);
	  break;
	default:
	  assert(false);
	}
	uint16_t index = Board<W, H>::CoordsToIndex(x, y);
	if (piece != FieldState::EMPTY && piece != FieldState::EMPTY_SURROUNDED_BY_BLACK && piece != FieldState::EMPTY_SURROUNDED_BY_WHITE && m_board->Chains[index] != index) {
	  bool foundChainDir = false;
	  for (int8_t dy = -1; dy <= 1 && !foundChainDir; ++dy) {
	    for (int8_t dx = -1; dx <= 1 && !foundChainDir; ++dx) {
	      if (dx == 0 && dy == 0) {
		continue;
	      }
	      if (x < -dx || y < -dy || x >= W-dx || y>= H-dy) {
		continue;
	      }
	      uint16_t nIndex = Board<W, H>::CoordsToIndex(x + dx, y + dy);
	      if (!PiecesMayBelongToSameChain(piece, m_board->Pieces[nIndex])) {
		continue;
	      }
	      if (m_board->Chains[nIndex] != m_board->Chains[index]) {
		continue;
	      }
	      if (m_board->Winding[index] - dx * ((int16_t)y+(int16_t)y+dy) == m_board->Winding[nIndex]) {
		std::cerr << "Win " << m_board->Winding[index] << "\n";
		foundChainDir = true;
		if (pieceSide == Side::BLACK) {
		  fl_color(FL_WHITE);
		} else {
		  fl_color(FL_BLACK);
		}
		std::cerr << "Found dir " << (int)dx << "," << (int)dy << "\n";
		DrawArrow(centerX, centerY, fieldWidth / 2, dx, dy);		
	      } else {
		std::cerr << "  " << (m_board->Winding[index]) << " + " << (dx * ((int16_t)y+(int16_t)y+dy)) << "!=" << m_board->Winding[nIndex] << "\n";
	      }
	    }
	  }
	  if (!foundChainDir) {
	    std::cerr << "Did not find chain dir at " << x << "," << y << " " << m_board->Winding[index] << "\n";
	  }
	}
      }
    }
  }

  void DrawArrow(float cx, float cy, float length, float dx, float dy) {
    fl_line(cx + (-length * dx/ 2), cy + (-length * dy/2), cx + length * dx/2, cy+length *dy/2);
    DrawFilledCircle(cx+length*dx/2, cy+length*dy/2, length / 4);
  }

  void DrawFilledCircle(float cx, float cy, float radius) {
    fl_pie(cx - radius, cy - radius, 2*radius, 2*radius, 0, 360);
  }

  int handle(int event) override {
    if (event == FL_PUSH) {
      const int x = (Fl::event_x() - m_x) * W / m_width;
      const int y = (Fl::event_y() - m_y) * H / m_height;
      std::cout << "xy " << x << "," << y << "\n";
      MakeMove(x, y);
      return 1;
    }
    return 0;
  }

  void MakeMove(int x, int y) {
    m_board->Place(x, y);
    redraw();
  }

private:
  int m_x, m_y;
  int m_width, m_height;
  Board<W, H>* m_board;
};

int main(int argc, char **argv) {
  Board<16,16>::InitializeHashes();
  Fl_Window *window = new Fl_Window(1024,768);
  Board<16, 16> gameBoard;
  Fl_Board<16, 16> *box = new Fl_Board<16, 16>(20,20,640,640,&gameBoard);
  //  box->box(FL_UP_BOX);
  //  box->labelfont(FL_BOLD+FL_ITALIC);
  //  box->labelsize(36);
  //  box->labeltype(FL_SHADOW_LABEL);
  window->end();
  window->show(argc, argv);
  return Fl::run();
}
