#include <iostream>

#include <atomic>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>

#include "board.h"

#include "../pieces.h"
//#include "../mcts.h"
#include "../mcts_engines.h"
#include "../pcg_random.hpp"

using kropki::Board;

template<class Board>
bool OnMoveMade(int x, int y, Board* gameBoard, Fl_Board<Board>* boardView);

int main(int argc, char*argv[]) {
  
  constexpr uint32_t W=10,H=10;
  pcg64 rand;
  Board<W,H>::InitializeHashes(rand);

  Fl::lock();
  Fl_Window* window = new Fl_Window(1024, 768);

  Fl_Board<Board<W, H>>* boardView = new Fl_Board<Board<W, H>>(20, 20, 640, 640, "");
  Board<W, H>* gameBoard = boardView->board();
  gameBoard->Place(W / 2 - 1, H / 2 - 1, Side::WHITE);
  gameBoard->Place(W / 2, H / 2, Side::WHITE);
  gameBoard->Place(W / 2 - 1, H / 2, Side::BLACK);
  gameBoard->Place(W / 2, H / 2 - 1, Side::BLACK);

  boardView->addOnMoveCallback([gameBoard, boardView](int x, int y) {
      return OnMoveMade(x, y, gameBoard, boardView);
    });
  window->end();
  window->show(argc, argv);
  return Fl::run();
  return 0;
}

template<class Board>
bool OnMoveMade(int x, int y, Board* gameBoard, Fl_Board<Board>* boardView) {
  if (!gameBoard->CanBePlayed(x, y)) {
    return false;
  }
  gameBoard->Place(x, y);
  boardView->setLastMove(x, y);

  if (gameBoard->NumEmptyFields == 0) {
    return false;
  }
  return true;
  boardView->acceptMoves(false);

  std::atomic_bool doneThinking;
  doneThinking.store(false);
  uint16_t bestMove = std::numeric_limits<uint16_t>::max();
  std::thread([gameBoard, &bestMove, &doneThinking]() {
		MCTS_UCT0_Engine<Board> mcts(*gameBoard);

		mcts.ThinkForTime(std::chrono::milliseconds(1000));
		bestMove = mcts.BestMove();
		doneThinking.store(true);
		Fl::awake();
	      }).detach();
  boardView->redraw();
  while (!doneThinking.load()) {
    Fl::wait();
  }
  X_ASSERT(bestMove != std::numeric_limits<uint16_t>::max());
  if (bestMove > 0) {
    boardView->acceptMoves(true);
    boardView->setLastMove(gameBoard->IndexToX(bestMove), gameBoard->IndexToY(bestMove));
    gameBoard->Place(bestMove);
    boardView->redraw();
  }
  return false;
}
