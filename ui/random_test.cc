#include <cstdio>

#include "board.h"

#include "../kropki.h"
#include "../kropki_debug.h"
#include "../pcg_random.hpp"


using kropki::Board;

int main(int argc, char **argv) {
  constexpr uint32_t W = 32, H = 16;
  pcg64 gen;
  Board<W,H>::InitializeHashes(gen);

  int matchNo = 0;
  while (true) {
    if (matchNo % 1000 == 0) {
      printf("New game: %d\n", matchNo);
    }
    matchNo++;
    Board<W, H> board;
    while (board.NumEmptyFields > 0) {
      std::uniform_int_distribution<> dis(0, board.NumEmptyFields-1);
      size_t moveIx = dis(gen);
      uint16_t move = board.GetEmptyFields()[moveIx];
      Board<W, H> boardCopy = board;
      boardCopy.Place(move);
      std::swap(board, boardCopy);
    }
  }
  return 0;
}
