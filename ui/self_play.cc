#include <iostream>

#include <atomic>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>
#include "board.h"

#include "../kropki_parent.h"
//#include "../mcts.h"
#include "../mcts_engines.h"
#include "../pcg_random.hpp"

using kropki_parent::Board;

int main(int argc, char*argv[]) {
  
  constexpr uint32_t W=10,H=10;
  pcg64 rand;
  //  Board<W,H>::InitializeHashes(rand);
  
  Fl::lock();
  Fl_Window* window = new Fl_Window(1024, 768);

  Fl_Board<Board<W, H>>* boardView = new Fl_Board<Board<W, H>>(20, 20, 640, 640, "");
  Board<W, H>* gameBoard = boardView->board();
  gameBoard->Place(W / 2 - 1, H / 2 - 1, Side::WHITE);
  gameBoard->Place(W / 2, H / 2, Side::WHITE);
  gameBoard->Place(W / 2 - 1, H / 2, Side::BLACK);
  gameBoard->Place(W / 2, H / 2 - 1, Side::BLACK);
  MCTS_UCT0_Engine<Board<W, H>> mcts(*gameBoard);
  std::thread t([&mcts, gameBoard, boardView]() {
      Side side = Side::WHITE;
      while (gameBoard->NumEmptyFields > 0) {
	uint16_t bestMove = std::numeric_limits<uint16_t>::max();
	std::cerr << "Iters: " << mcts.ThinkForTime(std::chrono::milliseconds(10000)) << "\n";
	bestMove = mcts.BestMove();
	if (bestMove == 0) {
	  std::cerr << (side == Side::WHITE ? "WHITE" : "BLACK") << " resigned\n";
	  return;
	}
	side = OppositeSide(side);
	mcts.Place(bestMove);
	Fl::lock();
	gameBoard->Place(bestMove);
	boardView->setLastMove(gameBoard->IndexToX(bestMove), gameBoard->IndexToY(bestMove));
	Fl::unlock();
	Fl::awake();
	boardView->redraw();
	X_ASSERT(bestMove != std::numeric_limits<uint16_t>::max());
      }
    });
  t.detach();
  window->end();
  window->show(argc, argv);
  return Fl::run();
}

