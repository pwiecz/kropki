#include <FL/Fl.H>
#include <FL/Fl_Window.H>

#include "board.h"

#include "../pcg_random.hpp"
#include "../kropki.h"
#include "../kropki_debug.h"

using kropki::Board;

int main(int argc, char **argv) {
  pcg64 gen;
  Board<16,16>::InitializeHashes(gen);
  Fl_Window *window = new Fl_Window(1024,768);
  Board<16, 16> gameBoard;
  new Fl_Board<16, 16>(20,20,640,640,&gameBoard);
  window->end();
  window->show(argc, argv);
  return Fl::run();
  return 0;
}
