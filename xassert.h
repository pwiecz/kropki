#pragma once

#include <string>
#include <exception>


class AssertionFailedException : public std::exception {
 public:
 AssertionFailedException(const std::string& str,
			  const std::string& file, 
			  int line, 
			  const std::string& func) 
   : Expr(str)
   , File(file)
   , Line(line)
   , Func(func) {}

  std::string Expr;
  std::string File;
  int Line;
std::string Func;
  
};

#define MY_ASSERT(e)                                             \
 ((e) ?                                                       \
  static_cast<void> (0) :                                     \
  throw AssertionFailedException(#e,  __FILE__,  __LINE__,  __FUNCTION__) \
 )

#if defined NDEBUG
# define X_ASSERT(CHECK) void(0)
#else
# define X_ASSERT(CHECK) \
    ( (CHECK) ? void(0) : []{MY_ASSERT(!#CHECK);}() )
#endif

